//
// xmega twi for as5013 2d linear magnetic encoder
// sec, 2017

#include "serial.h"
#include <avr/io.h>
#include <util/delay.h>
#include <avr/pgmspace.h>
#include <avr/interrupt.h>
#include "twi_master_driver.h"

#define SLAVE_ADDRESS    0x41
#define CPU_SPEED       32000000
#define TWI_BAUDRATE  100000
#define TWI_BAUDSETTING TWI_BAUD(CPU_SPEED, TWI_BAUDRATE)
TWI_Master_t twiMaster;    /*!< TWI master module. */

USART_data_t USART_data;

uint32_t value; //magnetic encoder reading
unsigned char Data_LSB, Data_MSB; //high byte and low byte of reading


uint8_t address_data[2]  = {0x00, 0x01};
uint8_t speed_mode_data[2] = {0x02, 0x04}; //set into slow mode, value=4
uint8_t sensitivity_mode_data[2] = {0x0B, 0x02}; //set into highest sensitivity value=2
bool result = true;




int main(void) {
   // set up clock
   OSC.CTRL = OSC_RC32MEN_bm; // enable 32MHz clock
   while (!(OSC.STATUS & OSC_RC32MRDY_bm)); // wait for clock to be ready
   CCP = CCP_IOREG_gc; // enable protected register change
   CLK.CTRL = CLK_SCLKSEL_RC32M_gc; // switch to 32MHz clock

   //set up usart
   PORTD.DIRSET = PIN3_bm; //TXD0
   PORTD.DIRCLR = PIN2_bm; //RXD0
   USART_InterruptDriver_Initialize(&USART_data, &USARTD0, USART_DREINTLVL_LO_gc);
   USART_Format_Set(USART_data.usart, USART_CHSIZE_8BIT_gc,
                     USART_PMODE_DISABLED_gc, 0);
   USART_RxdInterruptLevel_Set(USART_data.usart, USART_RXCINTLVL_LO_gc);
   //take f_sysclk/(BSEL+1) ~= f_baud*16 with zero scale.  See manual or spreadsheet for scale defs
   USART_Baudrate_Set(&USARTD0, 123 , -4); //230400 baud with .08% error
   USART_Rx_Enable(USART_data.usart);
   USART_Tx_Enable(USART_data.usart);
   //enable interrupts
   PMIC.CTRL |= PMIC_LOLVLEX_bm;

   //set up twi
   TWI_MasterInit(&twiMaster, &TWIC, TWI_MASTER_INTLVL_LO_gc, TWI_BAUDSETTING);
   //PORTC.DIRCLR = PIN0_bm | PIN1_bm; //TXD0

   sei();


   //set as5013 speed mode
   //result = TWI_MasterWrite(&twiMaster, SLAVE_ADDRESS, &speed_mode_data[0], 2);
   //while (twiMaster.status != TWIM_STATUS_READY) {}

   //set as5011 sensitivity
   //result = TWI_MasterWrite(&twiMaster, SLAVE_ADDRESS, &sensitivity_mode_data[0],2);
   //while (twiMaster.status != TWIM_STATUS_READY) {}


   while(1){

      result = TWI_MasterWriteRead(&twiMaster, SLAVE_ADDRESS, &address_data[0], 1,2); // Read D7..0
      while (twiMaster.status != TWIM_STATUS_READY) {}
      if (result) {
         Data_LSB = twiMaster.readData[0];
         Data_MSB = twiMaster.readData[1];

      }
      else { Data_LSB = 255; Data_MSB = 255;}

      //value = ((Data_MSB & 0x03)<<8) + Data_LSB;

      usart_send_byte(&USART_data,1);
      usart_send_byte(&USART_data,2);
      usart_send_byte(&USART_data,3);
      usart_send_byte(&USART_data,4);
      //usart_send_uint32(&USART_data,value);
      usart_send_byte(&USART_data,Data_LSB);
      //usart_send_byte(&USART_data,twiMaster.result);
      usart_send_byte(&USART_data,(Data_MSB & 0x03));
      usart_send_byte(&USART_data,10);
      _delay_ms(100);
   }
}

ISR(USARTD0_RXC_vect){USART_RXComplete(&USART_data);}
ISR(USARTD0_DRE_vect){USART_DataRegEmpty(&USART_data);}
ISR(TWIC_TWIM_vect){TWI_MasterInterruptHandler(&twiMaster);}
