#!/usr/bin/env python
import numpy as np
from math import *
import serial, time
import struct
import sys
import socket
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import bitstring

#packet format
n_samples = 6
n_framing = 8

#grid of values
Nx = 100
Ny = 100
positions = np.zeros((Nx,Ny,2))


def roll_left(lst):
	return lst[1:] + [lst[0]]
def read_as5013_12bit(bytes):
	return (bitstring.Bits(bytes=bytes, length=16)<<4).unpack('uint:16')[0]
	#signed 12 bit val comes in two bytes
	#return struct.unpack("<h",bytearray([ (high<<4) , low<<4 ]))[0]
def read_int16(bytes,signed=False):
	try:
		n = struct.unpack("<h",bytearray(bytes))[0] if signed else struct.unpack("<H",bytearray(bytes))[0]
		return n
	except:
		print "not properly formatted bytes: ",list(bytes)
		return None
def read_int8(bytes,signed=False):
	try:
		n = struct.unpack("<b",bytearray(bytes))[0] if signed else struct.unpack("<B",bytearray(bytes))[0]
		return n
	except:
		print "not properly formatted bytes: ",list(bytes)
		return None


def read(ser=None):
	if ser is None:
		print "No serial connection!"
	else:
		#wait for framing signal at tail of packet
		framing = range(n_framing) #we expect the sequence 012...
		c = [ser.read(1) for i in range(n_samples+n_framing)]
		#c += [ser.read(1) for i in framing]
		while( map(ord,c[n_samples:]) != framing ):
			c = roll_left(c)
			c[-1] = ser.read(1)
		#print c
		#c = ser.read(n_samples+12) #samples (20) + xi (2) + yi (2) + framing (8)
		try:
			data = [read_int8(c[x:x+1],signed=True) for x in xrange(2)]
			data += [read_int16(c[x:x+2],signed=False) for x in xrange(2,n_samples,2)]
		except(bitstring.CreationError):
			print c
		return data

def main():
	try:
		#make sure timeout is longer than any period between data.
		ser = serial.Serial(port='/dev/tty.usbserial-FT9L3KWR4',baudrate=1000000,timeout=5.)
		print "Established serial connection"
		ser.flush()
		while True: 
			try:
				data = np.asarray(read(ser=ser))
				print data
				#if (data[10]<Nx) and (data[11]<Ny):
				positions[data[2],data[3],0] = data[0]
				positions[data[2],data[3],1] = data[1]
			except(KeyboardInterrupt):
				ser.close()
				break
	except(OSError):
		#no serial port
		print "Couldn't find the serial port."
		ser = None			
	print "saving data"
	
	np.save('xy_readings.npy',positions)
	print "quitting"

if __name__ == '__main__':
	main()
