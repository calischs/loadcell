# Loadcells

Force measurement is almost always done by measuring the strain of a structure.  Most commonly, this is done using a strain gauge, which is bonded to a surface of a structural member.  With good bonds, the strain seen by the member is also seen by the strain gauge and a change is resistance is measured.  This approach has long been favored because it produces measurable results while allowing the strained member to remain as stiff as possible (and hence, not interfere with the operation of the measured device).

We can also measure strain of a member by measuring the displacement of a point or surface, rather than measuring the material strain directly.  This can be advantageous because it eliminates uncertainty from the bond between the member and a strain gauge.  These bonds can exhibit hysteresis, can be temperature dependent, and can be a point of failure of the device.  If we measure the displacement of a point or surface using a non-contact method, we can circumvent many of these disadvantages.

Non-contact position measurements have historically not had high enough resolution for load cell applications, but have been used commonly for applications that can tolerate larger displacements.  For instance, joystick inputs for machines are often sensed using a non-contact magnetic field measurement (c.f. US5831596A).  Similarly, many automotive applications (gas pedals, shifter positions, etc.) that sense large displacements are turning to non-contact measurement for the robustness it offers.  

Advances in high sensitivity measurement has made it possible to use non-contact measurement for the small displacements required by force sensing applications.  For instance, capacitive sensing is used in high resolution digital calipers and has found use in load cells (US10582023).  We breifly describe a prototype 3 axis capacitive load cell in a section below.  One downside to these techniques is that relatively large overlapping areas are required for a low noise measurement.  In many compact load cell applications (particularly for measuring many degrees of freedom), realizing these geometries can be challenging.

## Capacitive

One way to determine the proximity of two surfaces is by measuring the capacitance between them.  <a href='http://fab.cba.mit.edu/classes/862.16/people/sam.calisch/project/index.html'>This page</a> documents experiments with a 3 DOF Capacitive load cell using a single printed circuit board.

<img src='capacitive/milled.jpg' width=33%>
<img src='capacitive/settingup.jpg' width=39%>

It is based on a discrete electrostatics solver (shown below), and also documented on the page above.

<img src='capacitive/moving-dialectric.mp4' width=400px>

## Magnetic

Another position measurement uses the magnetic field instead of the electric field.  These devices consist of one or more magnetic field generating devices and one or more magnetic field sensing devices, usually arranged in pairs on opposing sides of a flexure.  The magnetic field generating devices are arrangements of permanent magnets or electrically driven coils.  The magnetic field sensing devices are usually spatial arrays of sensor elements, the measurements of which can be combined differentially to nullify external fields or unintentional displacements.  The sensing devices are usually positioned at the region of greatest rate of change of magnetic field strength with respect to the degrees of freedom of the flexure.

### Magnetic sensing technologies

Among magnetic field sensing technologies, Hall effect sensors are the most ubiquitous.  Integrated circuits with arrays of hall effect sensors are available at extremely low cost in very dense packages.  Using differential pairs of these elements, non-contact rotary and linear encoders can be made.  A great resource for designing such magnetic devices is the <a href='https://sensing.honeywell.com/hallbook.pdf'>Honeywell Hall Effect Handbook</a>.  Austrian Microsystems makes a variety of these devices for sensing position or rotation of a magnet.

For more sensitive, low field devices magnetoresistive sensing elements are often used.  <a href='http://www.physics.nyu.edu/kentlab/Lectures/Pappas_Tutorial_APSMM2008.pdf'>David Pappas of NIST compares</a> noise floors of these technologies, noting Hall effect sensors register around 300,000 $`pT/\sqrt{Hz}`$, while magnetoresistive show roughly 200 $`pT/\sqrt{Hz}`$.  Honeywell sells a variety of very sensitive magnetoresistive magnetometers, and NVE Corporation sells sensors based on giant magnetoresistance for very low field measurement.  Despite these favorable properties, sensors with these technologies are usually more expensive and less dense than those using the Hall effect.

A similar description holds for fluxgate technology, which can achieve the lowest noise of room temperature magnetic field sensing (10 $`pT/\sqrt{Hz}`$), according to Pappas.  The sensors achieving this specification are often of cubic centimeter volume, and cost thousands of dollars (c.f. Barrington).  Some have been miniaturized, like Texas Instruments' DRV425, but this sensor only reports 1500 $`pT/\sqrt{Hz}`$.  

<a href='https://link.springer.com/content/pdf/10.1023%2FB%3AJMSE.0000011350.93694.91.pdf'>Giant magnetoinductance</a> sensors are based on changes in skin depth of a signal due to magnetic field, and show low-noise, low-cost sensing (c.f. https://www.scirp.org/journal/PaperInformation.aspx?PaperID=36471).  Interesting prototypes have been built, but nothing is commercially available.

Finally, <a href='http://aip.scitation.org/doi/pdf/10.1063/1.2836410'>magneto-electric sensors</a> are a nascent technology with exciting prospects (100 $`pT/\sqrt{Hz}`$ according to Pappas).  In these sensors, a magnetostrictive material (usually a metglas) is combined with a piezoelectric material (usually lead zirconate titonate) into stacked layers.  The magnetic field produces strain in the stack, and the piezoelectric materials produce a corresponding voltage that can be read.  There is a <a href='https://www.nature.com/nmat/journal/v7/n2/pdf/nmat2106.pdf
'>great Nature Materials paper</a> demonstrating that a common SMD 1206 ceramic capacitor can be used as a 1 cent magnetic field sensor based on this physical phenomenon.  Again, this technology is promising, but not yet commercially available.

### Advantages

* As described above, the non-contact, flexural nature of these sensing devices means the output can be free from most sources of hysteresis.  
* These devices can be made so all electrically connect parts of the device are on one side of a flexure, and no cables, slip rings, etc. are required to bridge a spatially varying gap.  
* Hall effect elements are easily produced in arrays, allowing differential measurement across many axes, nullifying external fields and unwanted components of displacement.  This means we can use large gain values, creating very sensitive devices.
* These gain values are also adjustable on-the-fly, which broadens working dynamic range.
* The digital interfaces to these sensors can be efficiently bussed because the measurement time (100-1000 microseconds, typically) is much longer than the transmission time (10 microseconds, typically).  Thus, we can use many sensors on the same bus with little timing overhead.
* Temperature sensor circuits can also be included on these busses, offering fine-grained temperature compensation.
* Because the magnetic field generating and sensing devices are compact, the flexures can be designed to be easily manufacturable and have nice features like over travel limits.


### AS5510 1 DOF Loadcell

Here, we document experiments with a 1 DOF magnetic load cell, based on the Austrian microsystems AS5510 10 bit linear encoder IC, which boasts 500nm resolution for $1.41 (Q: 100).

<figure>
	<img src='as5510/as5011-loadcell.jpg' height=300px>
	<img src='as5510/pcb.png' height=300px>
</figure>

The PCB is mounted to a piece of waterjet aluminum with a flexure cut into it.  A magnet sits in a hole on the moving part of the flexure.  I designed the flexure to be roughly 5 um/Newton.  The encoder wants a air gap of less than 1mm, so I milled away the board inside the SOIC-8 footprint and flipped the part over to bring it closer to the magnet underneath.

I'm using an 8mm circular diametrically magnetized magnet.  We could swap in a smaller magnet to push the resolution of the prototype, but I started with the datasheet's recommended magnet form factor.  I'm running the chip at its highest sensitivity (+-12.5 mT full scale) and in slow mode (12.5 KHz as opposed to 50 KHz, but .5 mT peak-to-peak noise as opposed to .8 mTpp).

The graphs below show instron characterization of the prototype.  First, the flexure had a stiffness of 4.6 um/N, close to the designed 5 um/N.  Second, the time series comparison is very boring because the two series are so identical.  We see that a displacement of 400 um (corresponding to 100N) takes the encoder nearly over its full range (512 is the full range, as it measures positive and negative displacement to 10 bits (1024)).

<figure>
	<img src='as5510/instron-timeseries.png' height=300px>
	<img src='as5510/loadcell-timeseries.png' height=300px>
<figcaption>Time series.</figcaption>
</figure>

Finally, we can plot the relation between the two variables (force and hall reading).  Running at the full 12.5 kHz (i.e., without any summing of samples), we get about 5x LSB per Newton.  This isn't amazing resolution, but it is very much good enough for my application.  Further, we can easily take a hit in speed to get an increase in resolution by summing (i.e., averaging) samples, depending on the applciation.

<figure>
	<img src='as5510/loadcell-reading-v-force.png' height=400px>
</figure>


### 6 DOF Loadcell

A concept for a 6 degree of freedom magnetic load cell is shown below.  It uses a pair of flexures (aluminum or titanium plates on top and bottom) to set relative stiffnesses of Fx, Fy, Fz, Tx, Ty, Tz.  A central rod carries a disk with four small neodymium magnets which moves when loads are applied.  Four AS5013 hall array ICs are positioned just below the rest positions of the magnets on a single PCB.  M3 standoffs and screws hold the entire stack together.

<img src='v1/img/screenshot1.png' width=400px>

Features of a "good" 6 DOF flexure:
* Ability to tune relative stiffnesses (e.g. Fx/Fz, Tx/Tz, etc.)
* Minimal cross talk (e.g. Fx doesn't induce a displacement in y or a twist, etc.)
* Fits into a reasonably small bounding volume.

Fusion 360 lets us quickly test loading cases for flexure designs.  Below are simulations of a force and a moment applied to the loadcell.
<img src='v1/img/flexure.mp4' width=300px>
<img src='v1/img/flexure-twist.mp4' width=300px>


Building on the experiments with the AS5510, we explore the use of the AS5013 ($4.42 in Q100) to perform 3-dimensional tracking of a magnet.  This IC is nominally an 8-bit 2 axis magnetic encoder, though it contains 5 hall effect sensors, each with 12-bit resolution, adjustable gains, and I2C-accessible raw values.  <a href='as5013-test'>as5013-test/</a> contains pcb source files for a board to evaluate this measurement.  

<img src='as5013-test/block-diagram.png' width=350px>
<img src='as5013-test/shape-functions.jpg' width=400px>

To characterize the use of the AS5013 as a three-dimensional tracking device, we use a 5 axis stage (2 axes motorized, 3 manual) built around two Thorlabs PT1M micrometer stages.  After setting a magnet height and orientation, we can sample the 5 hall elements over a two dimensional array of magnet displacements.  Executing this for a handful of magnet heights should let us fit functions for X, Y, and Z in terms of the 5 hall readings.

<img src='as5013-test/testing2.jpg' width=350px>
<img src='as5013-test/nrf52-as5013-board.jpg' width=400px>

Below are some preliminary plots of readings from the 5 internal hall effect sensors.  They each run over 800 um with 10 um sample spacing.
<img src='as5013-test/hall-plots.png' width=1000px>
If we scan over a broader area, we can start to see deviations from an ideal radially symmetric field (there is also an overflow error in these plots that I haven't fixed yet).
<img src='as5013-test/hall-plots_full_view_aberrations.png' width=1000px>

This last plot indicates why the magnet spacing is 1.1mm -- from the center of the magnet, this is the approximate distance to the highest gradient in the z component of magnetic field.  This observation leads us to consider if we want to maximize resolution in tracking the magnet, could we make an alternative arrangements of magnet(s) to produce higher field gradients.

To test this, I wrote a very simple (and slow for now!) 2D B field finite difference simulation, which is in <a href='as5013-test/sim/'>sim</a>.  It uses successive overrelaxation to compute the spatial distribution of the z component of the magnetic field (todo: fix the field magnitude bug here).  Below we should output of the simulation for a single magnet, as well as for a pair of opposing magnets.  The first plot roughly confirms the shape and size of the magnetic field funtion from a single rod magnet.  

<img src='as5013-test/sim/run_single_magnet.png' width=350px>
<img src='as5013-test/sim/run_double_magnet.png' width=350px>

The pair of opposing magnets produces a high field gradient.  We could create such a region of high field gradient over each of the four outer hall effect sensors using a simple arrangement of a central magnet with a surrounding annulus of opposite polarity.  I EDM'ed a test arrangment out of a large neodymium magnet of 1/8" thickness.  This went reasonably well, except for a small tab near the lead-in which was left due to the brittleness of the magnet material.

<img src='magnet-arrangements/magnet-assembly-cuts.jpg' width=350px>
<img src='magnet-arrangements/magnet-assembly-3.jpg' width=350px>

I put this magnet arrangement on my stage to test the resolution.  I just grabbed the 8 bit X and Y values computed by the AS5013 and looked for the spatial range this mapped over.  Below are some very preliminary plots of this data, having not changed many of the parameters for the calculations.

<img src='as5013-test/xy-plots.png' width=800px>

This we extremely encouraging: we improved the resolution of this linear encoder to have a LSB of ~200nm!

I switched the stage into 1/8th microstepping mode (nominally 156 nm / step, but subject to backlash, etc.), and pulled out the raw sensor values.  We can create simple shape functions for the x and y displacement values as
```math
x = C_1 - C_2 + C_4 - C_3
```
```math
y = C_3 - C_2 + C_4 - C_1
```
Evaluating these, we can create the graphs below:
<img src='as5013-test/xy-plots-nonlinearity.png' width=800px>

These measurements are pulled from a single measurement on the sensor, so I expect averaging will reduce some of this nonlinearity.  I also want to play with the sensor gains to increase the region before the internal ADC overflows.  The goal of this will be to maximize dynamic range, which we could evaluate as the ratio of full scale to the standard deviation of the nonlinearity.  In the simple measurements above, this dynamic range is roughly 8 bits.  If we oversample 16x, we can get 10 bits at 150 Hz.  If we turn down the gains, we may work further above the hall elements' noise floors, additionally increasing dynamic range.  We also can turn on hall element $`C_5`$ which could increase dynamic range at no bandwidth penalty.

These measurements are also just at the edge of the positioning we can expect from our stage.  In fact, we can see the limits of stage repeatability in the graph at left, where an artifact shows at the left due to overscanning the grid and the misalignment between consecutive scans.  This ramifies in the nonlinearity measurement of the $`x`$ coordinate where we see large values up until the misalignment.  I take all this to mean we should hurry up and get to the full system before spending too much time on testing, but we might expect ~12 bits of dynamic range at ~150 Hz with ~100nm noise floor, given optimal parameter tuning. 

Below we show a new design incorporating this magnet change and rotating the ICs to give differential measurements in all axes.

<img src='v2/overall.png' width=480px>
<img src='v2/inside.png' width=310px>

I did a quick 3D print of this design to check clearance and assembly issues.  The flexure looks great (albeit obviously too compliant in the photopolymer material).

<img src='v2/eden-prototype.jpg' width=310px>
<img src='v2/v2-mill-max.png' width=310px>

Next task is designing the AS5013 board and its connection to the main board on the end of the cylinder.  I'm thinking of using the four position Mill-Max spring contact connectors to save space over a wired connector.  It should make a solid electrical connection for the I2C lines, power, and ground, while allowing room for fine adjustments of the sensor carrier boards.  It's going to be a tight fit getting around the corner.

<img src='v2/exploded-view-drawing.png' width=800px>

I prototyped the flexure out of .090" 304 stainless using our new FabLight 3kW fiber laser system.  The cut was much cleaner than I expected, and I may have to use this tool rather than spend hours at the EDM.

<img src='v2/flexure-fablight.jpg' width=800px>

I also managed to source cylinder and ring magnets of the appropriate dimensions from <a href='https://supermagnetman.com'>Super Magnet Man</a>.  This means we don't have to fabricate the concentric magnet pairs.  Below is an image of the new concentric pair next to the one I EDM'ed.

<img src='magnet-arrangements/supermagnetman-concentric.jpg' width=800px>

I designed the AS5013 daughter boards, using it as a chance to try the new integration of Eagle and Fusion360.  It's still pretty rough, but I like where it's headed, and it was certainly useful for this purpose.  I just grabbed the PCB outline geometry from Fusion, layed out my board in Eagle and pushed the changes back to Fusion to visualize the 3D circuit board in the context of my assembly.  Below are both views.  Pretty slick for "hobbyist cad"...  My only complaints are that the appearances are a little rough in Fusion (the via pads don't really overlap...) and the syncing process can be a little buggy.

<img src='as5013-node/as5013-eagle.png' width=360px>
<img src='as5013-node/as5013-fusion.png' width=600px>

I sent out for a run of these boards which should allow me to experiment with calibration and measurement, both for the 6 DOF configuration, as well as others.  The first image below shows four of these PCBs, soldered and arranged on a cnc machined body.  The second image shows one of the two flexure plates in place.  The last image shows the main pcb which sits on top, contacting the daughter boards through spring pin connectors, which allow for individual adjustment of their positions.

<img src='v2/body-with-pins.jpg' height=300px>
<img src='v2/body-with-flexure.jpg' height=300px>
<img src='v2/body-with-main-pcb.jpg' height=300px>

I did another bit of machining to turn out a few loadcell bodies and magnet carriers.  I'm happy with the toolpaths for these parts, so it should make it fast and easy to make additional versions with minor edits.

<img src='v2/carrier-with-magnet.jpg' height=300px>
<img src='v2/body-and-carrier.jpg' height=300px>

Next step is dialing in the flexure plates and producing them.  I'm not thrilled with the edge quality of the laser cut versions, so I think I may EDM them after all.  I'll design in over-travel limits to these to keep the flexures in their elastic range.


### Torque Cell

I've also been looking at a similar mechanism for a non-contact torque cell.  Most such torque measurement devices use a slip ring or radio telemetry to transmit the measured values to the stationary frame.  We can design a flexure (similar to a helical shaft coupling) with a portion that translates in the axial direction when subjected to torque.  Depending on requirements, we can dial up and down the stiffness and range.  We can mount an axially magnetized ring magnet (or better yet, an opposite pair of ring magnets) and measure the axial displacement using a pair of differential hall elements in the stationary frame.  This allows us to subtract out small deviations in the gap between the magnet and sensor.  Below are some quick simulations of a candidate flexure geometry:

<img src='torque-cell/tc-sim.mp4' width=300px>
<img src='torque-cell/tc-test-small.mp4' width=300px>

The video above shows a printed test of an exagerrated flexure (so that we can see the deflection).  I've also printed some much stiffer flexures which translate in a range measureable by a differential hall effect pair.

### 2 DOF Force + Moment

After talking to Ian about the requirements of a 2 degree of freedom load cell for a prosthetic ankle application, here was a quick sketch of a flexure and layout of magnets and sensors.

<img src='ankle-2dof/ankle-2dof-sketch-1.png' width=300px>
<img src='ankle-2dof/ankle-2dof-sketch-2.png' width=300px>

The two sensors can differentiate forces in the Z direction and moments around the X axis (out of the screen). 

<img src='ankle-2dof/ankle-2dof-sim-force.png' width=300px>
<img src='ankle-2dof/ankle-2dof-sim-moment.png' width=300px>

### Updates

I put together a load cell "dev kit" package to send to Mike at Moog:

<img src='img/loadcell-bodies.jpg' height=300px>
<img src='img/package.jpg' height=300px>


### Bipolar, nonlinear loadcells

I've become interested in nonlinear load cells -- those whose transfer function changes slope in order to be very sensitive at small loads and less sensitive at larger loads (in order to maximize range).  One way to think about this transfer function is as a soft transition into a protective stop.

Nonlinear load cells have been made by geometric design of flexures, as in this <a href="https://dspace.mit.edu/handle/1721.1/119789">thesis</a>. This is very cool, but seems it might be sensitive to contamination and the contact forces might exhibit some hysteresis.  It also doesn't seem to lend itself to miniaturization.

Another approach is to use a flexure that is more linear but to measure displacement using a physical principle with a nonlinear transfer function.  One example of such a principle is frustrated total internal reflection, as the reflectivity is a roughly logarithmic function of gap distance.  <a href='https://link.springer.com/article/10.1140/epje/i2017-11542-4'>This paper</a> demonstrates the measurement of film thickness using this principle (and as a bonus includes some force measurement based on Hertzian contact between planar and spherical lenses!).  

I think a bipolar nonlinear load cell could be made based on frustrated reflection that was very sensitive around the origin, while having a very large maximum range.  It could also be made extremely small and inexpensive (led, photodiode, a few miniature lenses, likely encapsulated in an oil).


