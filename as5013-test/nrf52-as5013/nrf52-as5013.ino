//read raw values from as5013 hall effect array ic and pass to uarte
//sec, 2017
#include "radio.h"

//as5013 registers
#define ctrl_reg 0x0F
#define y_res_reg 0x11
#define T_ctrl_reg 0x2D
#define agc_reg 0x2A
#define c_raw_values_start 0x16 //starting address for raw sensor values
#define n_tx_bytes 1 //only need transmit the start read address
#define n_rx_bytes 20 //20 values pos_low, pos_high, neg_low, neg_high for each of c1, c2, c3, c4, c5

#define scl_pin 0 //XL1
#define sda_pin 1 //XL2
#define rst_pin 3 //A1
static uint8_t txdata[n_tx_bytes] = {0};
static uint8_t rxdata[n_rx_bytes+12] = {0}; //add four bytes for xi, yi over uarte, 8 bytes for framing


//uarte
const uint8_t pin_rx = 8;
const uint8_t pin_tx = 6;
uint32_t last_tx_time = 0;
uint32_t tx_period = 200; //ms

int16_t step_period = 3;

//
//UARTE
//
void uarte_setup(){
  //uart with dma
  NRF_UARTE0->PSEL.TXD = (pin_tx << UARTE_PSEL_TXD_PIN_Pos) & UARTE_PSEL_TXD_PIN_Msk;
  NRF_UARTE0->PSEL.RXD = (pin_rx << UARTE_PSEL_RXD_PIN_Pos) & UARTE_PSEL_RXD_PIN_Msk;
  NRF_UARTE0->CONFIG =  ((UART_CONFIG_PARITY_Excluded << UARTE_CONFIG_PARITY_Pos) & UARTE_CONFIG_PARITY_Msk) 
                      | ((UARTE_CONFIG_HWFC_Disabled << UARTE_CONFIG_HWFC_Pos) & UARTE_CONFIG_HWFC_Msk);
  NRF_UARTE0->BAUDRATE = UART_BAUDRATE_BAUDRATE_Baud1M;
  NRF_UARTE0->ENABLE = (UARTE_ENABLE_ENABLE_Enabled << UARTE_ENABLE_ENABLE_Pos) & UARTE_ENABLE_ENABLE_Msk;
  
  NRF_UARTE0->TXD.MAXCNT = n_rx_bytes+12;
}


//
//TWI
//
void twi_set_cnts(uint8_t n_tx, uint8_t n_rx){
  NRF_TWIM0->TXD.MAXCNT = (n_tx << TWIM_TXD_MAXCNT_MAXCNT_Pos) & TWIM_TXD_MAXCNT_MAXCNT_Msk;
  NRF_TWIM0->RXD.MAXCNT = (n_rx << TWIM_RXD_MAXCNT_MAXCNT_Pos) & TWIM_RXD_MAXCNT_MAXCNT_Msk;  
}
void twi_setup(){
  //Need to switch to internal LFCLK to disconnect from XL1 and XL2
  NRF_CLOCK->LFCLKSRC = 0; //disconnect XL1 AND XL2 FROM LFCLK?
  NRF_CLOCK->EVENTS_LFCLKSTARTED = 0;
  NRF_CLOCK->TASKS_LFCLKSTART    = 1;
  while (NRF_CLOCK->EVENTS_LFCLKSTARTED == 0){}

  NRF_TWIM0->ENABLE = (TWI_ENABLE_ENABLE_Disabled << TWI_ENABLE_ENABLE_Pos) & TWI_ENABLE_ENABLE_Msk;
  NRF_GPIO->PIN_CNF[scl_pin] = (GPIO_PIN_CNF_DRIVE_S0D1 << GPIO_PIN_CNF_DRIVE_Pos) & GPIO_PIN_CNF_DRIVE_Msk;
  NRF_GPIO->PIN_CNF[sda_pin] = (GPIO_PIN_CNF_DRIVE_S0D1 << GPIO_PIN_CNF_DRIVE_Pos) & GPIO_PIN_CNF_DRIVE_Msk;
  NRF_GPIO->DIRCLR = (1<<scl_pin)|(1<<sda_pin); //set SDA/SCL as inputs (likely not necessary)

  NRF_TWIM0->PSEL.SCL = ((scl_pin << TWIM_PSEL_SCL_PIN_Pos) & TWIM_PSEL_SCL_PIN_Msk)
                      | ((TWIM_PSEL_SCL_CONNECT_Connected << TWIM_PSEL_SCL_CONNECT_Pos) & TWIM_PSEL_SCL_CONNECT_Msk);
  NRF_TWIM0->PSEL.SDA = ((sda_pin << TWIM_PSEL_SDA_PIN_Pos) & TWIM_PSEL_SDA_PIN_Msk)
                      | ((TWIM_PSEL_SDA_CONNECT_Connected << TWIM_PSEL_SDA_CONNECT_Pos) & TWIM_PSEL_SDA_CONNECT_Msk);
  NRF_TWIM0->FREQUENCY = (TWI_FREQUENCY_FREQUENCY_K100 << TWI_FREQUENCY_FREQUENCY_Pos) & TWI_FREQUENCY_FREQUENCY_Msk;
  NRF_TWIM0->ADDRESS = (0x41 << TWI_ADDRESS_ADDRESS_Pos) & TWI_ADDRESS_ADDRESS_Msk;
  NRF_TWIM0->TXD.PTR = (uint32_t)(&txdata);
  NRF_TWIM0->RXD.PTR = (uint32_t)(&rxdata);
  twi_set_cnts(n_tx_bytes, n_rx_bytes);
  //set up short between LASTTX and STOP and between LASTRX and STOP
  NRF_TWIM0->SHORTS = ((TWIM_SHORTS_LASTTX_STOP_Enabled << TWIM_SHORTS_LASTTX_STOP_Pos) & TWIM_SHORTS_LASTTX_STOP_Msk)
                    | ((TWIM_SHORTS_LASTRX_STOP_Enabled << TWIM_SHORTS_LASTRX_STOP_Pos) & TWIM_SHORTS_LASTRX_STOP_Msk);
  //NRF_TWIM0->ENABLE = (TWI_ENABLE_ENABLE_Enabled << TWI_ENABLE_ENABLE_Pos) & TWI_ENABLE_ENABLE_Msk;
  //There's a typo in nrf52_bitfields, so we set this manually.
  NRF_TWIM0->ENABLE = (6 << TWI_ENABLE_ENABLE_Pos) & TWI_ENABLE_ENABLE_Msk;
}
void twi_tx(){
  // clear the stopped event
  NRF_TWIM0->EVENTS_STOPPED = 0;
  // triggering the STARTTX task
  NRF_TWIM0->TASKS_STARTTX = 1;
  while( !(NRF_TWIM0->EVENTS_STOPPED) ){}
}
void twi_rx(){
  // clear the stopped event
  NRF_TWIM0->EVENTS_STOPPED = 0;
  // trigger the STARTRX task
  NRF_TWIM0->TASKS_STARTRX = 1;
  while( !(NRF_TWIM0->EVENTS_STOPPED) ){}
}


//
//AS5013
//
void as5013_setup(){
  NRF_GPIO->DIRSET = (1<<rst_pin); //set AS5013 nRESET pin as output
  NRF_GPIO->OUTCLR = (1<<rst_pin); //hardware reset
  delayMicroseconds(10);
  NRF_GPIO->OUTSET = (1<<rst_pin); //turn on as5013
  delayMicroseconds(1000);

  //Loop check ctrl_reg until the value F0h or F1h is present
  delay(1); //in practice even no delay is fine.
  //config sensors for maximum gain
  twi_set_cnts(2, 0);
  txdata[0] = agc_reg;
  txdata[1] = 0x3F; //gain value, 0x3F maximum
  twi_tx(); 
}
void as5013_read(){
  //assume idle mode, so we must read from Y_res_int register to trigger new conversion
  twi_set_cnts(1, 1);
  txdata[0] = y_res_reg;
  twi_tx();
  twi_rx(); 
  delay(1);
  //read raw values
  twi_set_cnts(n_tx_bytes, n_rx_bytes);
  txdata[0] = c_raw_values_start; //set up to read from this address
  twi_tx();
  twi_rx();
}

void configure_motor(){
  //set step speed
  radio_buffer[0] = 2; //set step speed
  radio_buffer[1] = step_period*1000; //10ms step period
  radio_send_redundant();
  //set full steps
  radio_buffer[0] = 3; //set full steps
  radio_buffer[1] = 0; //M1,M0 = 0,0
  radio_send_redundant();
  //set current limit
  radio_buffer[0] = 4; //set current limit
  radio_buffer[1] = 200; //M1,M0 = 0,1
  radio_send_redundant();
}

//
//main
//
void setup() {
  //Serial.begin(115200);
  uarte_setup();
  twi_setup();
  as5013_setup();
  radio_setup();
  
  NRF_RADIO->TXADDRESS   = 0x00UL;  // Set device address 0: X axis
  configure_motor();
  NRF_RADIO->TXADDRESS   = 0x01UL;  // Set device address 1: Y axis
  configure_motor();

  int16_t xi=0; //counter
  int16_t yi=0; //counter
  int16_t Nx = 100; //max number of x moves
  int16_t Ny = 100; //max number of y moves
  //int16_t moves_per_step = 8; //8 steps = 10 microns
  int16_t moves_per_step = 16; //8 steps = 10 microns

  //insert framing.
  rxdata[n_rx_bytes+4] = (uint8_t)0;
  rxdata[n_rx_bytes+5] = (uint8_t)1;
  rxdata[n_rx_bytes+6] = (uint8_t)2;
  rxdata[n_rx_bytes+7] = (uint8_t)3;
  rxdata[n_rx_bytes+8] = (uint8_t)4;
  rxdata[n_rx_bytes+9] = (uint8_t)5;
  rxdata[n_rx_bytes+10] = (uint8_t)6;
  rxdata[n_rx_bytes+11] = (uint8_t)7;
    
  while(1){
    as5013_read();
    rxdata[n_rx_bytes] = (uint8_t)(xi);
    rxdata[n_rx_bytes+1] = (uint8_t)(xi>>8);
    rxdata[n_rx_bytes+2] = (uint8_t)(yi);
    rxdata[n_rx_bytes+3] = (uint8_t)(yi>>8);
    NRF_UARTE0->TXD.PTR = (uint32_t)(&rxdata);  //reset pointer to start of buffer
    NRF_UARTE0->TASKS_STARTTX = 1;  //trigger start task to send data to host

    delay(1); //for demonstration, not really necessary.
    
    NRF_RADIO->TXADDRESS   = 0x00UL;  // Set device address 0: X axis
    radio_buffer[0] = 1; //send a move command
    radio_buffer[1] = moves_per_step; //move 10 steps forward
    radio_send_redundant(); //send command
    delay(step_period*moves_per_step+1);
    xi++;
    
    if (xi==Nx){
      xi=0;
      NRF_RADIO->TXADDRESS   = 0x00UL;  // Set device address 0: X axis
      radio_buffer[0] = 1; //send a move command
      radio_buffer[1] = -moves_per_step*Nx; //move back to start
      radio_send_redundant(); //send command  
      delay(step_period*Nx*moves_per_step+1);
      
      NRF_RADIO->TXADDRESS   = 0x01UL;  // Set device address 1: Y axis
      radio_buffer[0] = 1; //send a move command
      radio_buffer[1] = moves_per_step; //move back to start
      radio_send_redundant(); //send command  
      delay(step_period*moves_per_step+1);   
      yi++;     
    }
    if (yi==Ny){
      delay(10);
      yi=0;
      NRF_RADIO->TXADDRESS   = 0x01UL;  // Set device address 1: Y axis
      radio_buffer[0] = 1; //send a move command
      radio_buffer[1] = -moves_per_step*Ny; //move back to start
      radio_send_redundant(); //send command  
      delay(step_period*Ny*moves_per_step+1);
    }
  }
}


void loop() {}
