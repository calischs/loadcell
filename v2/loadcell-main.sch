<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="8.4.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="no" active="no"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="no"/>
<layer number="103" name="tMap" color="7" fill="1" visible="no" active="no"/>
<layer number="104" name="Name" color="7" fill="1" visible="no" active="no"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="no" active="no"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="no" active="no"/>
<layer number="107" name="Crop" color="7" fill="1" visible="no" active="no"/>
<layer number="108" name="fp8" color="7" fill="1" visible="no" active="no"/>
<layer number="109" name="fp9" color="7" fill="1" visible="no" active="no"/>
<layer number="110" name="110" color="7" fill="1" visible="no" active="no"/>
<layer number="111" name="111" color="7" fill="1" visible="no" active="no"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="no" active="no"/>
<layer number="113" name="IDFDebug" color="7" fill="1" visible="no" active="yes"/>
<layer number="114" name="FRNTMAAT1" color="7" fill="1" visible="no" active="yes"/>
<layer number="115" name="FRNTMAAT2" color="7" fill="1" visible="no" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="no"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="no" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="no"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="no" active="yes"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="no" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="no"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="no"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="no"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="no"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="129" name="top_silk" color="7" fill="1" visible="no" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="no" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="no"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="no"/>
<layer number="133" name="bottom_silk" color="7" fill="1" visible="no" active="yes"/>
<layer number="144" name="DrillLegend" color="7" fill="1" visible="no" active="no"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="14" fill="1" visible="no" active="no"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="no" active="no"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="no"/>
<layer number="201" name="201bmp" color="2" fill="1" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="1" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="no"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="no"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="no"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="no"/>
<layer number="207" name="207bmp" color="15" fill="10" visible="no" active="no"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="no"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="no" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="no" active="no"/>
<layer number="249" name="Edge" color="7" fill="1" visible="no" active="no"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="OrgLBR" color="13" fill="1" visible="no" active="no"/>
<layer number="255" name="Accent" color="7" fill="1" visible="no" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="sec" urn="urn:adsk.eagle:library:725670">
<packages>
<package name="MILLMAX-22-004-40-001101" urn="urn:adsk.eagle:footprint:725719/1" locally_modified="yes" library_version="7" library_locally_modified="yes">
<description>Mill max smd spring pin connector, 4 pos

http://www.mouser.com/ds/2/273/010-259781.pdf</description>
<smd name="1" x="-3.81" y="1.27" dx="2.1336" dy="2.54" layer="1"/>
<smd name="2" x="-1.27" y="1.27" dx="2.1336" dy="2.54" layer="1"/>
<smd name="3" x="1.27" y="1.27" dx="2.1336" dy="2.54" layer="1"/>
<smd name="4" x="3.81" y="1.27" dx="2.1336" dy="2.54" layer="1"/>
<circle x="-4.445" y="-0.381" radius="0.254" width="0" layer="21"/>
<wire x1="-5.08" y1="0" x2="5.08" y2="0" width="0.254" layer="51"/>
<wire x1="5.08" y1="0" x2="5.08" y2="7.62" width="0.254" layer="51"/>
<wire x1="5.08" y1="7.62" x2="-5.08" y2="7.62" width="0.254" layer="51"/>
<wire x1="-5.08" y1="7.62" x2="-5.08" y2="0" width="0.254" layer="51"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="5.08" width="0.254" layer="21"/>
<wire x1="-5.08" y1="5.08" x2="-5.08" y2="2.54" width="0.254" layer="21"/>
</package>
<package name="MILLMAX-22-004-40-001101_RECV" urn="urn:adsk.eagle:footprint:725719/1" locally_modified="yes" library_version="7" library_locally_modified="yes">
<description>Mill max smd spring pin connector, 4 pos

http://www.mouser.com/ds/2/273/010-259781.pdf</description>
<smd name="1" x="-3.81" y="0" dx="2.032" dy="2.032" layer="1" roundness="100"/>
<smd name="2" x="-1.27" y="0" dx="2.032" dy="2.032" layer="1" roundness="100"/>
<smd name="3" x="1.27" y="0" dx="2.032" dy="2.032" layer="1" roundness="100"/>
<smd name="4" x="3.81" y="0" dx="2.032" dy="2.032" layer="1" roundness="100"/>
<circle x="-4.699" y="1.778" radius="0.254" width="0" layer="21"/>
<wire x1="-5.08" y1="-1.27" x2="5.08" y2="-1.27" width="0.254" layer="51"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.254" layer="51"/>
<wire x1="5.08" y1="1.27" x2="-5.08" y2="1.27" width="0.254" layer="51"/>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="-1.27" width="0.254" layer="51"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.254" layer="21"/>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="-1.27" width="0.254" layer="21"/>
</package>
<package name="C0805" urn="urn:adsk.eagle:footprint:739321/1" library_version="7" library_locally_modified="yes">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.381" y1="0.66" x2="0.381" y2="0.66" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.66" x2="0.381" y2="-0.66" width="0.1016" layer="51"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0922" y1="-0.7239" x2="-0.3421" y2="0.7262" layer="51"/>
<rectangle x1="0.3556" y1="-0.7239" x2="1.1057" y2="0.7262" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="TAG-CONNECT" library_version="7" library_locally_modified="yes">
<hole x="0" y="0" drill="0.99821875"/>
<hole x="5.08" y="1.016" drill="0.99821875"/>
<hole x="5.08" y="-1.016" drill="0.99821875"/>
<smd name="P$1" x="1.27" y="-0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100"/>
<smd name="P$2" x="1.27" y="0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100"/>
<smd name="P$4" x="2.54" y="0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100"/>
<smd name="P$6" x="3.81" y="0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100"/>
<smd name="P$5" x="3.81" y="-0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100"/>
<smd name="P$3" x="2.54" y="-0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100"/>
<wire x1="1.27" y1="-0.635" x2="3.81" y2="-0.635" width="0.127" layer="39"/>
<wire x1="3.81" y1="-0.635" x2="3.81" y2="0.635" width="0.127" layer="39"/>
<wire x1="3.81" y1="0.635" x2="1.27" y2="0.635" width="0.127" layer="39"/>
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.127" layer="39"/>
<circle x="0" y="0" radius="0.5334" width="0.066040625" layer="46"/>
<circle x="0" y="0" radius="0.5334" width="0.066040625" layer="46"/>
<circle x="5.08" y="1.016" radius="0.5334" width="0.066040625" layer="46"/>
<circle x="5.08" y="-1.016" radius="0.5334" width="0.066040625" layer="46"/>
</package>
<package name="1206_MILL" library_version="7" library_locally_modified="yes">
<description>&lt;b&gt;0805&lt;/b&gt;chip&lt;p&gt;

0805 (imperial)&lt;br/&gt;
2012 (metric)&lt;br/&gt;
IPC Nominal Density</description>
<smd name="1" x="-1.5875" y="0" dx="1.524" dy="2.032" layer="1"/>
<smd name="2" x="1.5875" y="0" dx="1.524" dy="2.032" layer="1"/>
<text x="-1" y="1.119" size="0.762" layer="25">&gt;NAME</text>
<text x="-1" y="-1.865" size="0.762" layer="27">&gt;VALUE</text>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
<wire x1="-1.5875" y1="0.762" x2="1.5875" y2="0.762" width="0.127" layer="51"/>
<wire x1="1.5875" y1="0.762" x2="1.5875" y2="-0.762" width="0.127" layer="51"/>
<wire x1="1.5875" y1="-0.762" x2="-1.5875" y2="-0.762" width="0.127" layer="51"/>
<wire x1="-1.5875" y1="-0.762" x2="-1.5875" y2="0.762" width="0.127" layer="51"/>
</package>
<package name="TACTILE_SWITCH_KMR7" library_version="7" library_locally_modified="yes">
<smd name="P$1" x="-2" y="-0.8" dx="2" dy="1" layer="1"/>
<smd name="P$2" x="2" y="-0.8" dx="2" dy="1" layer="1"/>
<smd name="P$3" x="-2" y="0.8" dx="2" dy="1" layer="1"/>
<smd name="P$4" x="2" y="0.8" dx="2" dy="1" layer="1"/>
<circle x="-2.8" y="-1.5" radius="0.1" width="0" layer="21"/>
<wire x1="-2.2" y1="1.5" x2="-2.2" y2="-1.5" width="0.127" layer="51"/>
<wire x1="-2.2" y1="-1.5" x2="2.2" y2="-1.5" width="0.127" layer="51"/>
<wire x1="2.2" y1="-1.5" x2="2.2" y2="1.5" width="0.127" layer="51"/>
<wire x1="2.2" y1="1.5" x2="-2.2" y2="1.5" width="0.127" layer="51"/>
<wire x1="-2.3" y1="0.2" x2="-2.3" y2="-0.2" width="0.127" layer="21"/>
<wire x1="2.3" y1="0.2" x2="2.3" y2="-0.2" width="0.127" layer="21"/>
<wire x1="-0.9" y1="-1.3" x2="0.9" y2="-1.3" width="0.127" layer="21"/>
<wire x1="-0.9" y1="1.3" x2="0.9" y2="1.3" width="0.127" layer="21"/>
</package>
<package name="FANSTEL_BC832_SHRINKD" library_version="7" library_locally_modified="yes">
<smd name="P$1" x="0" y="-0.482" dx="1.3" dy="0.65" layer="1"/>
<smd name="P$2" x="0" y="-1.582" dx="1.3" dy="0.65" layer="1"/>
<smd name="P$3" x="0" y="-2.682" dx="1.3" dy="0.65" layer="1"/>
<smd name="P$4" x="0" y="-3.782" dx="1.3" dy="0.65" layer="1"/>
<smd name="P$5" x="0" y="-4.882" dx="1.3" dy="0.65" layer="1"/>
<smd name="P$6" x="0" y="-5.982" dx="1.3" dy="0.65" layer="1"/>
<smd name="P$7" x="0" y="-7.082" dx="1.3" dy="0.65" layer="1"/>
<smd name="P$8" x="0" y="-8.182" dx="1.3" dy="0.65" layer="1"/>
<smd name="P$9" x="7.8" y="-8.182" dx="1.3" dy="0.65" layer="1"/>
<smd name="P$10" x="7.8" y="-7.082" dx="1.3" dy="0.65" layer="1"/>
<smd name="P$11" x="7.8" y="-5.982" dx="1.3" dy="0.65" layer="1"/>
<smd name="P$12" x="7.8" y="-4.882" dx="1.3" dy="0.65" layer="1"/>
<smd name="P$13" x="7.8" y="-3.782" dx="1.3" dy="0.65" layer="1"/>
<smd name="P$14" x="7.8" y="-2.682" dx="1.3" dy="0.65" layer="1"/>
<smd name="P$15" x="7.8" y="-1.582" dx="1.3" dy="0.65" layer="1"/>
<smd name="P$16" x="7.8" y="-0.482" dx="1.3" dy="0.65" layer="1"/>
<smd name="P$17" x="2.15" y="-8.281" dx="0.6" dy="0.6" layer="1" roundness="100"/>
<smd name="P$18" x="2.15" y="-7.081" dx="0.6" dy="0.6" layer="1" roundness="100"/>
<smd name="P$19" x="2.15" y="-5.881" dx="0.6" dy="0.6" layer="1" roundness="100"/>
<smd name="P$20" x="2.15" y="-4.681" dx="0.6" dy="0.6" layer="1" roundness="100"/>
<smd name="P$21" x="2.15" y="-3.481" dx="0.6" dy="0.6" layer="1" roundness="100"/>
<smd name="P$22" x="3.35" y="-8.281" dx="0.6" dy="0.6" layer="1" roundness="100"/>
<smd name="P$23" x="3.35" y="-7.081" dx="0.6" dy="0.6" layer="1" roundness="100"/>
<smd name="P$24" x="3.35" y="-5.881" dx="0.6" dy="0.6" layer="1" roundness="100"/>
<smd name="P$25" x="3.35" y="-4.681" dx="0.6" dy="0.6" layer="1" roundness="100"/>
<smd name="P$26" x="3.35" y="-3.481" dx="0.6" dy="0.6" layer="1" roundness="100"/>
<smd name="P$27" x="4.55" y="-8.281" dx="0.6" dy="0.6" layer="1" roundness="100"/>
<smd name="P$28" x="4.55" y="-7.081" dx="0.6" dy="0.6" layer="1" roundness="100"/>
<smd name="P$29" x="4.55" y="-5.881" dx="0.6" dy="0.6" layer="1" roundness="100"/>
<smd name="P$30" x="4.55" y="-4.681" dx="0.6" dy="0.6" layer="1" roundness="100"/>
<smd name="P$31" x="4.55" y="-3.481" dx="0.6" dy="0.6" layer="1" roundness="100"/>
<smd name="P$32" x="5.75" y="-8.281" dx="0.127" dy="0.127" layer="1" roundness="100"/>
<smd name="P$33" x="5.75" y="-7.081" dx="0.127" dy="0.127" layer="1" roundness="100"/>
<smd name="P$34" x="5.75" y="-5.881" dx="0.127" dy="0.127" layer="1" roundness="100"/>
<smd name="P$35" x="5.75" y="-4.681" dx="0.127" dy="0.127" layer="1" roundness="100"/>
<smd name="P$36" x="5.75" y="-3.481" dx="0.127" dy="0.127" layer="1" roundness="100"/>
<circle x="-0.8" y="-0.25" radius="0.1" width="0" layer="21"/>
<wire x1="0" y1="0" x2="0" y2="-8.8" width="0.127" layer="51"/>
<wire x1="0" y1="-8.8" x2="7.8" y2="-8.8" width="0.127" layer="51"/>
<wire x1="7.8" y1="-8.8" x2="7.8" y2="0" width="0.127" layer="21"/>
<wire x1="0" y1="0" x2="7.8" y2="0" width="0.127" layer="51"/>
<wire x1="0" y1="-0.95" x2="0" y2="-1.1" width="0.127" layer="21"/>
<wire x1="0" y1="-2.05" x2="0" y2="-2.2" width="0.127" layer="21"/>
<wire x1="0" y1="-3.15" x2="0" y2="-3.3" width="0.127" layer="21"/>
<wire x1="0" y1="-4.25" x2="0" y2="-4.4" width="0.127" layer="21"/>
<wire x1="0" y1="-5.35" x2="0" y2="-5.5" width="0.127" layer="21"/>
<wire x1="0" y1="-6.45" x2="0" y2="-6.6" width="0.127" layer="21"/>
<wire x1="0" y1="-7.55" x2="0" y2="-7.7" width="0.127" layer="21"/>
<wire x1="7.8" y1="-7.55" x2="7.8" y2="-7.7" width="0.127" layer="21"/>
<wire x1="7.8" y1="-6.45" x2="7.8" y2="-6.6" width="0.127" layer="21"/>
<wire x1="7.8" y1="-5.35" x2="7.8" y2="-5.5" width="0.127" layer="21"/>
<wire x1="7.8" y1="-4.25" x2="7.8" y2="-4.4" width="0.127" layer="21"/>
<wire x1="7.8" y1="-3.15" x2="7.8" y2="-3.3" width="0.127" layer="21"/>
<wire x1="7.8" y1="-2.05" x2="7.8" y2="-2.2" width="0.127" layer="21"/>
<wire x1="7.8" y1="-0.95" x2="7.8" y2="-1.1" width="0.127" layer="21"/>
<wire x1="0" y1="0" x2="7.8" y2="0" width="0.127" layer="21"/>
<wire x1="0" y1="-8.8" x2="7.8" y2="-8.8" width="0.127" layer="21"/>
<text x="1.397" y="-2.159" size="1" layer="25">&gt;NAME</text>
</package>
<package name="FANSTEL_BC832" library_version="7" library_locally_modified="yes">
<smd name="P$1" x="0" y="-0.482" dx="1.3" dy="0.6" layer="1"/>
<smd name="P$2" x="0" y="-1.582" dx="1.3" dy="0.6" layer="1"/>
<smd name="P$3" x="0" y="-2.682" dx="1.3" dy="0.6" layer="1"/>
<smd name="P$4" x="0" y="-3.782" dx="1.3" dy="0.6" layer="1"/>
<smd name="P$5" x="0" y="-4.882" dx="1.3" dy="0.6" layer="1"/>
<smd name="P$6" x="0" y="-5.982" dx="1.3" dy="0.6" layer="1"/>
<smd name="P$7" x="0" y="-7.082" dx="1.3" dy="0.6" layer="1"/>
<smd name="P$8" x="0" y="-8.182" dx="1.3" dy="0.6" layer="1"/>
<smd name="P$9" x="7.8" y="-8.182" dx="1.3" dy="0.6" layer="1"/>
<smd name="P$10" x="7.8" y="-7.082" dx="1.3" dy="0.6" layer="1"/>
<smd name="P$11" x="7.8" y="-5.982" dx="1.3" dy="0.6" layer="1"/>
<smd name="P$12" x="7.8" y="-4.882" dx="1.3" dy="0.6" layer="1"/>
<smd name="P$13" x="7.8" y="-3.782" dx="1.3" dy="0.6" layer="1"/>
<smd name="P$14" x="7.8" y="-2.682" dx="1.3" dy="0.6" layer="1"/>
<smd name="P$15" x="7.8" y="-1.582" dx="1.3" dy="0.6" layer="1"/>
<smd name="P$16" x="7.8" y="-0.482" dx="1.3" dy="0.6" layer="1"/>
<smd name="P$17" x="2.15" y="-8.281" dx="0.6" dy="0.6" layer="1" roundness="100"/>
<smd name="P$18" x="2.15" y="-7.081" dx="0.6" dy="0.6" layer="1" roundness="100"/>
<smd name="P$19" x="2.15" y="-5.881" dx="0.6" dy="0.6" layer="1" roundness="100"/>
<smd name="P$20" x="2.15" y="-4.681" dx="0.6" dy="0.6" layer="1" roundness="100"/>
<smd name="P$21" x="2.15" y="-3.481" dx="0.6" dy="0.6" layer="1" roundness="100"/>
<smd name="P$22" x="3.35" y="-8.281" dx="0.6" dy="0.6" layer="1" roundness="100"/>
<smd name="P$23" x="3.35" y="-7.081" dx="0.6" dy="0.6" layer="1" roundness="100"/>
<smd name="P$24" x="3.35" y="-5.881" dx="0.6" dy="0.6" layer="1" roundness="100"/>
<smd name="P$25" x="3.35" y="-4.681" dx="0.6" dy="0.6" layer="1" roundness="100"/>
<smd name="P$26" x="3.35" y="-3.481" dx="0.6" dy="0.6" layer="1" roundness="100"/>
<smd name="P$27" x="4.55" y="-8.281" dx="0.6" dy="0.6" layer="1" roundness="100"/>
<smd name="P$28" x="4.55" y="-7.081" dx="0.6" dy="0.6" layer="1" roundness="100"/>
<smd name="P$29" x="4.55" y="-5.881" dx="0.6" dy="0.6" layer="1" roundness="100"/>
<smd name="P$30" x="4.55" y="-4.681" dx="0.6" dy="0.6" layer="1" roundness="100"/>
<smd name="P$31" x="4.55" y="-3.481" dx="0.6" dy="0.6" layer="1" roundness="100"/>
<smd name="P$32" x="5.75" y="-8.281" dx="0.6" dy="0.6" layer="1" roundness="100"/>
<smd name="P$33" x="5.75" y="-7.081" dx="0.6" dy="0.6" layer="1" roundness="100"/>
<smd name="P$34" x="5.75" y="-5.881" dx="0.6" dy="0.6" layer="1" roundness="100"/>
<smd name="P$35" x="5.75" y="-4.681" dx="0.6" dy="0.6" layer="1" roundness="100"/>
<smd name="P$36" x="5.75" y="-3.481" dx="0.6" dy="0.6" layer="1" roundness="100"/>
<circle x="-0.8" y="-0.25" radius="0.1" width="0" layer="21"/>
<wire x1="0" y1="0" x2="0" y2="-8.8" width="0.127" layer="51"/>
<wire x1="0" y1="-8.8" x2="7.8" y2="-8.8" width="0.127" layer="51"/>
<wire x1="7.8" y1="-8.8" x2="7.8" y2="0" width="0.127" layer="51"/>
<wire x1="0" y1="0" x2="7.8" y2="0" width="0.127" layer="51"/>
<wire x1="0" y1="-0.95" x2="0" y2="-1.1" width="0.127" layer="21"/>
<wire x1="0" y1="-2.05" x2="0" y2="-2.2" width="0.127" layer="21"/>
<wire x1="0" y1="-3.15" x2="0" y2="-3.3" width="0.127" layer="21"/>
<wire x1="0" y1="-4.25" x2="0" y2="-4.4" width="0.127" layer="21"/>
<wire x1="0" y1="-5.35" x2="0" y2="-5.5" width="0.127" layer="21"/>
<wire x1="0" y1="-6.45" x2="0" y2="-6.6" width="0.127" layer="21"/>
<wire x1="0" y1="-7.55" x2="0" y2="-7.7" width="0.127" layer="21"/>
<wire x1="7.8" y1="-7.55" x2="7.8" y2="-7.7" width="0.127" layer="21"/>
<wire x1="7.8" y1="-6.45" x2="7.8" y2="-6.6" width="0.127" layer="21"/>
<wire x1="7.8" y1="-5.35" x2="7.8" y2="-5.5" width="0.127" layer="21"/>
<wire x1="7.8" y1="-4.25" x2="7.8" y2="-4.4" width="0.127" layer="21"/>
<wire x1="7.8" y1="-3.15" x2="7.8" y2="-3.3" width="0.127" layer="21"/>
<wire x1="7.8" y1="-2.05" x2="7.8" y2="-2.2" width="0.127" layer="21"/>
<wire x1="7.8" y1="-0.95" x2="7.8" y2="-1.1" width="0.127" layer="21"/>
<wire x1="0" y1="0" x2="7.8" y2="0" width="0.127" layer="21"/>
<wire x1="0" y1="-8.8" x2="7.8" y2="-8.8" width="0.127" layer="21"/>
<text x="1.397" y="-2.159" size="1" layer="25">&gt;NAME</text>
<wire x1="0.762" y1="0.254" x2="0.762" y2="-3.048" width="0.127" layer="41"/>
<wire x1="0.762" y1="-3.048" x2="6.985" y2="-3.048" width="0.127" layer="41"/>
<wire x1="6.985" y1="-3.048" x2="6.985" y2="0.254" width="0.127" layer="41"/>
<wire x1="6.985" y1="0.254" x2="0.762" y2="0.254" width="0.127" layer="41"/>
<wire x1="0.762" y1="-3.048" x2="6.985" y2="-3.048" width="0.127" layer="42"/>
<wire x1="6.985" y1="-3.048" x2="6.985" y2="0.254" width="0.127" layer="42"/>
<wire x1="6.985" y1="0.254" x2="0.762" y2="0.254" width="0.127" layer="42"/>
<wire x1="0.762" y1="0.254" x2="0.762" y2="-3.048" width="0.127" layer="42"/>
</package>
<package name="TSOT-23-5" library_version="7" library_locally_modified="yes">
<smd name="P$1" x="-1.31" y="0.95" dx="1.3" dy="0.5" layer="1"/>
<smd name="P$2" x="-1.31" y="0" dx="1.3" dy="0.5" layer="1"/>
<smd name="P$3" x="-1.31" y="-0.95" dx="1.3" dy="0.5" layer="1"/>
<smd name="P$4" x="1.31" y="-0.95" dx="1.3" dy="0.5" layer="1"/>
<smd name="P$5" x="1.31" y="0.95" dx="1.3" dy="0.5" layer="1"/>
<circle x="-1.8" y="1.4" radius="0.1" width="0" layer="21"/>
<wire x1="-0.6" y1="0.9" x2="0.6" y2="0.9" width="0.127" layer="21"/>
<wire x1="-0.6" y1="-0.9" x2="0.6" y2="-0.9" width="0.127" layer="21"/>
<wire x1="0.7" y1="-0.6" x2="0.7" y2="0.6" width="0.127" layer="21"/>
<wire x1="-0.7" y1="-0.6" x2="-0.7" y2="-0.4" width="0.127" layer="21"/>
<wire x1="-0.7" y1="0.6" x2="-0.7" y2="0.4" width="0.127" layer="21"/>
</package>
<package name="RESC0201_L" library_version="7" library_locally_modified="yes">
<description>&lt;b&gt;0201&lt;/b&gt; chip&lt;p&gt;
0201 (imperial)&lt;br/&gt;
0603 (metric)&lt;br/&gt;
IPC High Density</description>
<smd name="1" x="-0.255" y="0" dx="0.28" dy="0.43" layer="1"/>
<smd name="2" x="0.255" y="0" dx="0.28" dy="0.43" layer="1"/>
<text x="-2" y="1" size="1.27" layer="25">&gt;NAME</text>
<text x="-2" y="-2" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-0.3" y1="0.15" x2="-0.3" y2="-0.15" width="0.1" layer="51"/>
<wire x1="-0.3" y1="-0.15" x2="0.3" y2="-0.15" width="0.1" layer="51"/>
<wire x1="0.3" y1="-0.15" x2="0.3" y2="0.15" width="0.1" layer="51"/>
<wire x1="0.3" y1="0.15" x2="-0.3" y2="0.15" width="0.1" layer="51"/>
<wire x1="-0.5" y1="-0.3" x2="0.5" y2="-0.3" width="0.1" layer="39"/>
<wire x1="0.5" y1="-0.3" x2="0.5" y2="0.3" width="0.1" layer="39"/>
<wire x1="0.5" y1="0.3" x2="-0.5" y2="0.3" width="0.1" layer="39"/>
<wire x1="-0.5" y1="0.3" x2="-0.5" y2="-0.3" width="0.1" layer="39"/>
</package>
<package name="RESC0201_M" library_version="7" library_locally_modified="yes">
<description>&lt;b&gt;0201&lt;/b&gt; chip&lt;p&gt;
0201 (imperial)&lt;br/&gt;
0603 (metric)&lt;br/&gt;
IPC Low Density</description>
<smd name="1" x="-0.355" y="0" dx="0.5" dy="0.55" layer="1"/>
<smd name="2" x="0.355" y="0" dx="0.5" dy="0.55" layer="1"/>
<text x="-0.4" y="1.1" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-0.3" y1="0.15" x2="-0.3" y2="-0.15" width="0.1" layer="51"/>
<wire x1="-0.3" y1="-0.15" x2="0.3" y2="-0.15" width="0.1" layer="51"/>
<wire x1="0.3" y1="-0.15" x2="0.3" y2="0.15" width="0.1" layer="51"/>
<wire x1="0.3" y1="0.15" x2="-0.3" y2="0.15" width="0.1" layer="51"/>
<wire x1="0.8" y1="-0.5" x2="0.8" y2="0.5" width="0.1" layer="39"/>
<wire x1="0.8" y1="0.5" x2="-0.8" y2="0.5" width="0.1" layer="39"/>
<wire x1="-0.8" y1="0.5" x2="-0.8" y2="-0.5" width="0.1" layer="39"/>
<wire x1="-0.8" y1="-0.5" x2="0.8" y2="-0.5" width="0.1" layer="39"/>
</package>
<package name="RESC0201_N" library_version="7" library_locally_modified="yes">
<description>&lt;b&gt;0201&lt;/b&gt; chip&lt;p&gt;
0201 (imperial)&lt;br/&gt;
0603 (metric)&lt;br/&gt;
IPC Nominal Density</description>
<smd name="1" x="-0.305" y="0" dx="0.4" dy="0.45" layer="1"/>
<smd name="2" x="0.305" y="0" dx="0.4" dy="0.45" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-0.3" y1="0.15" x2="-0.3" y2="-0.15" width="0.1" layer="51"/>
<wire x1="-0.3" y1="-0.15" x2="0.3" y2="-0.15" width="0.1" layer="51"/>
<wire x1="0.3" y1="-0.15" x2="0.3" y2="0.15" width="0.1" layer="51"/>
<wire x1="0.3" y1="0.15" x2="-0.3" y2="0.15" width="0.1" layer="51"/>
<wire x1="0.65" y1="-0.35" x2="0.65" y2="0.35" width="0.1" layer="39"/>
<wire x1="0.65" y1="0.35" x2="-0.65" y2="0.35" width="0.1" layer="39"/>
<wire x1="-0.65" y1="0.35" x2="-0.65" y2="-0.35" width="0.1" layer="39"/>
<wire x1="-0.65" y1="-0.35" x2="0.65" y2="-0.35" width="0.1" layer="39"/>
</package>
<package name="RESC0402_L" library_version="7" library_locally_modified="yes">
<description>&lt;b&gt;0402&lt;/b&gt; chip &lt;p&gt;

0402 (imperial)&lt;br/&gt;
1005 (metric)&lt;br/&gt;
IPC High Density</description>
<smd name="1" x="-0.5" y="0" dx="0.5" dy="0.6" layer="1"/>
<smd name="2" x="0.5" y="0" dx="0.5" dy="0.6" layer="1"/>
<text x="-1" y="1" size="1.27" layer="25">&gt;NAME</text>
<text x="-1" y="-2" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
<wire x1="-0.5" y1="0.25" x2="0.5" y2="0.25" width="0.1" layer="51"/>
<wire x1="0.5" y1="0.25" x2="0.5" y2="-0.25" width="0.1" layer="51"/>
<wire x1="0.5" y1="-0.25" x2="-0.5" y2="-0.25" width="0.1" layer="51"/>
<wire x1="-0.5" y1="-0.25" x2="-0.5" y2="0.25" width="0.1" layer="51"/>
<wire x1="0.85" y1="-0.4" x2="0.85" y2="0.4" width="0.1" layer="39"/>
<wire x1="0.85" y1="0.4" x2="-0.85" y2="0.4" width="0.1" layer="39"/>
<wire x1="-0.85" y1="0.4" x2="-0.85" y2="-0.4" width="0.1" layer="39"/>
<wire x1="-0.85" y1="-0.4" x2="0.85" y2="-0.4" width="0.1" layer="39"/>
</package>
<package name="RESC0402_M" library_version="7" library_locally_modified="yes">
<description>&lt;b&gt;0402&lt;/b&gt; chip &lt;p&gt;

0402 (imperial)&lt;br/&gt;
1005 (metric)&lt;br/&gt;
IPC Low Density</description>
<smd name="1" x="-0.6" y="0" dx="0.7" dy="0.7" layer="1"/>
<smd name="2" x="0.6" y="0" dx="0.7" dy="0.7" layer="1"/>
<text x="-1" y="1" size="1.27" layer="25">&gt;NAME</text>
<text x="-1" y="-2" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
<wire x1="-0.5" y1="0.25" x2="0.5" y2="0.25" width="0.1" layer="51"/>
<wire x1="0.5" y1="0.25" x2="0.5" y2="-0.25" width="0.1" layer="51"/>
<wire x1="0.5" y1="-0.25" x2="-0.5" y2="-0.25" width="0.1" layer="51"/>
<wire x1="-0.5" y1="-0.25" x2="-0.5" y2="0.25" width="0.1" layer="51"/>
<wire x1="1.15" y1="-0.55" x2="1.15" y2="0.55" width="0.1" layer="39"/>
<wire x1="1.15" y1="0.55" x2="-1.15" y2="0.55" width="0.1" layer="39"/>
<wire x1="-1.15" y1="0.55" x2="-1.15" y2="-0.55" width="0.1" layer="39"/>
<wire x1="-1.15" y1="-0.55" x2="1.15" y2="-0.55" width="0.1" layer="39"/>
</package>
<package name="RESC0402_N" library_version="7" library_locally_modified="yes">
<description>&lt;b&gt;0402&lt;/b&gt; chip &lt;p&gt;

0402 (imperial)&lt;br/&gt;
1005 (metric)&lt;br/&gt;
IPC Nominal Density</description>
<smd name="1" x="-0.55" y="0" dx="0.6" dy="0.6" layer="1"/>
<smd name="2" x="0.55" y="0" dx="0.6" dy="0.6" layer="1"/>
<text x="-0.6" y="1.1" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.7" y="-2" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
<wire x1="-0.5" y1="0.25" x2="0.5" y2="0.25" width="0.1" layer="51"/>
<wire x1="0.5" y1="0.25" x2="0.5" y2="-0.25" width="0.1" layer="51"/>
<wire x1="0.5" y1="-0.25" x2="-0.5" y2="-0.25" width="0.1" layer="51"/>
<wire x1="-0.5" y1="-0.25" x2="-0.5" y2="0.25" width="0.1" layer="51"/>
<wire x1="1" y1="-0.45" x2="1" y2="0.45" width="0.1" layer="39"/>
<wire x1="1" y1="0.45" x2="-1" y2="0.45" width="0.1" layer="39"/>
<wire x1="-1" y1="0.45" x2="-1" y2="-0.45" width="0.1" layer="39"/>
<wire x1="-1" y1="-0.45" x2="1" y2="-0.45" width="0.1" layer="39"/>
</package>
<package name="RESC0603_L" library_version="7" library_locally_modified="yes">
<description>&lt;b&gt;0603&lt;/b&gt; chip &lt;p&gt;

0603 (imperial)&lt;br/&gt;
1608 (metric)&lt;br/&gt;
IPC High Density</description>
<smd name="1" x="-0.7" y="0" dx="0.85" dy="0.8" layer="1"/>
<smd name="2" x="0.7" y="0" dx="0.85" dy="0.8" layer="1"/>
<text x="-1" y="1" size="1.27" layer="25">&gt;NAME</text>
<text x="-1" y="-2" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
<wire x1="-0.8" y1="0.4" x2="0.8" y2="0.4" width="0.127" layer="51"/>
<wire x1="0.8" y1="0.4" x2="0.8" y2="-0.4" width="0.127" layer="51"/>
<wire x1="0.8" y1="-0.4" x2="-0.8" y2="-0.4" width="0.127" layer="51"/>
<wire x1="-0.8" y1="-0.4" x2="-0.8" y2="0.4" width="0.127" layer="51"/>
<wire x1="1.25" y1="-0.5" x2="1.25" y2="0.5" width="0.1" layer="39"/>
<wire x1="1.25" y1="0.5" x2="-1.25" y2="0.5" width="0.1" layer="39"/>
<wire x1="-1.25" y1="0.5" x2="-1.25" y2="-0.5" width="0.1" layer="39"/>
<wire x1="-1.25" y1="-0.5" x2="1.25" y2="-0.5" width="0.1" layer="39"/>
</package>
<package name="RESC0603_M" library_version="7" library_locally_modified="yes">
<description>&lt;b&gt;0603&lt;/b&gt; chip &lt;p&gt;

0603 (imperial)&lt;br/&gt;
1608 (metric)&lt;br/&gt;
IPC High Density</description>
<smd name="1" x="-0.9" y="0" dx="1.25" dy="1" layer="1"/>
<smd name="2" x="0.9" y="0" dx="1.25" dy="1" layer="1"/>
<text x="-1" y="2" size="1.27" layer="25">&gt;NAME</text>
<text x="-1" y="-3" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
<wire x1="-0.8" y1="0.4" x2="0.8" y2="0.4" width="0.127" layer="51"/>
<wire x1="0.8" y1="0.4" x2="0.8" y2="-0.4" width="0.127" layer="51"/>
<wire x1="0.8" y1="-0.4" x2="-0.8" y2="-0.4" width="0.127" layer="51"/>
<wire x1="-0.8" y1="-0.4" x2="-0.8" y2="0.4" width="0.127" layer="51"/>
<wire x1="2" y1="-1" x2="2" y2="1" width="0.1" layer="39"/>
<wire x1="2" y1="1" x2="-2" y2="1" width="0.1" layer="39"/>
<wire x1="-2" y1="1" x2="-2" y2="-1" width="0.1" layer="39"/>
<wire x1="-2" y1="-1" x2="2" y2="-1" width="0.1" layer="39"/>
</package>
<package name="RESC0603_N" library_version="7" library_locally_modified="yes">
<description>&lt;b&gt;0603&lt;/b&gt; chip &lt;p&gt;

0603 (imperial)&lt;br/&gt;
1608 (metric)&lt;br/&gt;
IPC Nominal Density</description>
<smd name="1" x="-0.8" y="0" dx="1.05" dy="0.9" layer="1"/>
<smd name="2" x="0.8" y="0" dx="1.05" dy="0.9" layer="1"/>
<text x="-1" y="1" size="1.27" layer="25">&gt;NAME</text>
<text x="-1" y="-2.5" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
<wire x1="-0.8" y1="0.4" x2="0.8" y2="0.4" width="0.127" layer="51"/>
<wire x1="0.8" y1="0.4" x2="0.8" y2="-0.4" width="0.127" layer="51"/>
<wire x1="0.8" y1="-0.4" x2="-0.8" y2="-0.4" width="0.127" layer="51"/>
<wire x1="-0.8" y1="-0.4" x2="-0.8" y2="0.4" width="0.127" layer="51"/>
<wire x1="1.6" y1="-0.7" x2="1.6" y2="0.7" width="0.1" layer="39"/>
<wire x1="1.6" y1="0.7" x2="-1.6" y2="0.7" width="0.1" layer="39"/>
<wire x1="-1.6" y1="0.7" x2="-1.6" y2="-0.7" width="0.1" layer="39"/>
<wire x1="-1.6" y1="-0.7" x2="1.6" y2="-0.7" width="0.1" layer="39"/>
</package>
<package name="RESC0805_L" library_version="7" library_locally_modified="yes">
<description>&lt;b&gt;0805&lt;/b&gt;chip&lt;p&gt;

0805 (imperial)&lt;br/&gt;
2012 (metric)&lt;br/&gt;
IPC High Density</description>
<smd name="1" x="-1" y="0" dx="1" dy="1.25" layer="1"/>
<smd name="2" x="1" y="0" dx="1" dy="1.25" layer="1"/>
<text x="-1" y="1" size="0.508" layer="25">&gt;NAME</text>
<text x="-1" y="-1.23" size="0.508" layer="27">&gt;VALUE</text>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
<wire x1="-1" y1="0.6" x2="1" y2="0.6" width="0.127" layer="51"/>
<wire x1="1" y1="0.6" x2="1" y2="-0.6" width="0.127" layer="51"/>
<wire x1="1" y1="-0.6" x2="-1" y2="-0.6" width="0.127" layer="51"/>
<wire x1="-1" y1="-0.6" x2="-1" y2="0.6" width="0.127" layer="51"/>
<wire x1="1.6" y1="-0.75" x2="1.6" y2="0.75" width="0.1" layer="39"/>
<wire x1="1.6" y1="0.75" x2="-1.6" y2="0.75" width="0.1" layer="39"/>
<wire x1="-1.6" y1="0.75" x2="-1.6" y2="-0.75" width="0.1" layer="39"/>
<wire x1="-1.6" y1="-0.75" x2="1.6" y2="-0.75" width="0.1" layer="39"/>
</package>
<package name="RESC0805_N" library_version="7" library_locally_modified="yes">
<description>&lt;b&gt;0805&lt;/b&gt;chip&lt;p&gt;

0805 (imperial)&lt;br/&gt;
2012 (metric)&lt;br/&gt;
IPC Nominal Density</description>
<smd name="1" x="-1.1" y="0" dx="1.2" dy="1.35" layer="1"/>
<smd name="2" x="1" y="0" dx="1.2" dy="1.35" layer="1"/>
<text x="-1" y="0.738" size="0.508" layer="25">&gt;NAME</text>
<text x="-1" y="-1.23" size="0.508" layer="27">&gt;VALUE</text>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
<wire x1="-1" y1="0.6" x2="1" y2="0.6" width="0.127" layer="51"/>
<wire x1="1" y1="0.6" x2="1" y2="-0.6" width="0.127" layer="51"/>
<wire x1="1" y1="-0.6" x2="-1" y2="-0.6" width="0.127" layer="51"/>
<wire x1="-1" y1="-0.6" x2="-1" y2="0.6" width="0.127" layer="51"/>
<wire x1="1.85" y1="-0.95" x2="1.85" y2="0.95" width="0.1" layer="39"/>
<wire x1="1.85" y1="0.95" x2="-1.95" y2="0.95" width="0.1" layer="39"/>
<wire x1="-1.95" y1="0.95" x2="-1.95" y2="-0.95" width="0.1" layer="39"/>
<wire x1="-1.95" y1="-0.95" x2="1.85" y2="-0.95" width="0.1" layer="39"/>
</package>
<package name="RESC0805_M" library_version="7" library_locally_modified="yes">
<description>&lt;b&gt;0805&lt;/b&gt;chip&lt;p&gt;

0805 (imperial)&lt;br/&gt;
2012 (metric)&lt;br/&gt;
IPC Low Density</description>
<smd name="1" x="-1.2" y="0" dx="1.4" dy="1.45" layer="1"/>
<smd name="2" x="1.2" y="0" dx="1.4" dy="1.45" layer="1"/>
<text x="-2" y="0.73" size="0.508" layer="25">&gt;NAME</text>
<text x="-2" y="-1.73" size="0.508" layer="27">&gt;VALUE</text>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
<wire x1="-1" y1="0.6" x2="1" y2="0.6" width="0.127" layer="51"/>
<wire x1="1" y1="0.6" x2="1" y2="-0.6" width="0.127" layer="51"/>
<wire x1="1" y1="-0.6" x2="-1" y2="-0.6" width="0.127" layer="51"/>
<wire x1="-1" y1="-0.6" x2="-1" y2="0.6" width="0.127" layer="51"/>
<wire x1="2.4" y1="-1.2" x2="2.4" y2="1.2" width="0.1" layer="39"/>
<wire x1="2.4" y1="1.2" x2="-2.4" y2="1.2" width="0.1" layer="39"/>
<wire x1="-2.4" y1="1.2" x2="-2.4" y2="-1.2" width="0.1" layer="39"/>
<wire x1="-2.4" y1="-1.2" x2="2.4" y2="-1.2" width="0.1" layer="39"/>
</package>
<package name="JST-CONNECT-ACH-2" library_version="7" library_locally_modified="yes">
<smd name="P$1" x="4.2" y="-0.6" dx="1.75" dy="0.6" layer="1"/>
<smd name="P$2" x="4.2" y="0.6" dx="1.75" dy="0.6" layer="1"/>
<smd name="P$3" x="0.4" y="1.75" dx="1.5" dy="1" layer="1"/>
<smd name="P$4" x="0.4" y="-1.75" dx="1.5" dy="1" layer="1"/>
<wire x1="0" y1="-2" x2="4.2" y2="-2" width="0.127" layer="21"/>
<wire x1="4.2" y1="-2" x2="4.2" y2="2" width="0.127" layer="21"/>
<wire x1="4.2" y1="2" x2="0" y2="2" width="0.127" layer="21"/>
<wire x1="0" y1="2" x2="0" y2="1" width="0.127" layer="21"/>
<wire x1="0" y1="1" x2="1" y2="0" width="0.127" layer="21"/>
<wire x1="1" y1="0" x2="0" y2="-1" width="0.127" layer="21"/>
<wire x1="0" y1="-1" x2="0" y2="-2" width="0.127" layer="21"/>
<text x="1" y="1" size="0.5" layer="21">&gt;NAME</text>
<text x="1.016" y="-1.778" size="0.5" layer="21">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="MILLMAX-22-004-40-001101" urn="urn:adsk.eagle:package:725720/2" type="model" library_version="7" library_locally_modified="yes">
<description>Mill max smd spring pin connector, 4 pos

http://www.mouser.com/ds/2/273/010-259781.pdf</description>
</package3d>
</packages3d>
<symbols>
<symbol name="BC832" library_version="7" library_locally_modified="yes">
<pin name="P0.00/XL1@02" x="-5.08" y="38.1" length="middle"/>
<pin name="P0.01/XL2@03" x="-5.08" y="35.56" length="middle"/>
<pin name="P0.02/AIN0@04" x="-5.08" y="33.02" length="middle"/>
<pin name="P0.03/AIN1@05" x="-5.08" y="30.48" length="middle"/>
<pin name="P0.04/AIN2@06" x="-5.08" y="27.94" length="middle"/>
<pin name="P0.05/AIN3@07" x="-5.08" y="25.4" length="middle"/>
<pin name="P0.06@08" x="-5.08" y="22.86" length="middle"/>
<pin name="P0.07@09" x="-5.08" y="20.32" length="middle"/>
<pin name="P0.08@10" x="-5.08" y="17.78" length="middle"/>
<pin name="P0.09/NFC1@11" x="-5.08" y="15.24" length="middle"/>
<pin name="P0.10/NFC2@12" x="-5.08" y="12.7" length="middle"/>
<pin name="P0.11@14" x="15.24" y="-5.08" length="middle" rot="R90"/>
<pin name="P0.12@15" x="17.78" y="-5.08" length="middle" rot="R90"/>
<pin name="P0.13@16" x="20.32" y="-5.08" length="middle" rot="R90"/>
<pin name="P0.14@17" x="22.86" y="-5.08" length="middle" rot="R90"/>
<pin name="P0.15@18" x="25.4" y="-5.08" length="middle" rot="R90"/>
<pin name="P0.16@19" x="27.94" y="-5.08" length="middle" rot="R90"/>
<pin name="P0.17@20" x="30.48" y="-5.08" length="middle" rot="R90"/>
<pin name="P0.18/SWO@21" x="33.02" y="-5.08" length="middle" rot="R90"/>
<pin name="P0.19@22" x="35.56" y="-5.08" length="middle" rot="R90"/>
<pin name="P0.20@23" x="38.1" y="-5.08" length="middle" rot="R90"/>
<pin name="P0.21/RESET@24" x="40.64" y="-5.08" length="middle" rot="R90"/>
<pin name="SWDCLK@25" x="58.42" y="12.7" length="middle" direction="in" rot="R180"/>
<pin name="SWDIO@26" x="58.42" y="15.24" length="middle" rot="R180"/>
<pin name="P0.22@27" x="58.42" y="17.78" length="middle" rot="R180"/>
<pin name="P0.23@28" x="58.42" y="20.32" length="middle" rot="R180"/>
<pin name="P0.24@29" x="58.42" y="22.86" length="middle" rot="R180"/>
<pin name="P0.25@37" x="40.64" y="58.42" length="middle" rot="R270"/>
<pin name="P0.26@38" x="38.1" y="58.42" length="middle" rot="R270"/>
<pin name="P0.27@39" x="35.56" y="58.42" length="middle" rot="R270"/>
<pin name="P0.28@40" x="33.02" y="58.42" length="middle" rot="R270"/>
<pin name="P0.29@41" x="30.48" y="58.42" length="middle" rot="R270"/>
<pin name="VSS@45" x="20.32" y="58.42" length="middle" direction="pwr" rot="R270"/>
<pin name="DEC4@46" x="17.78" y="58.42" length="middle" direction="pwr" rot="R270"/>
<pin name="DCC@47" x="15.24" y="58.42" length="middle" direction="out" rot="R270"/>
<pin name="VDD@48" x="12.7" y="58.42" length="middle" direction="pwr" rot="R270"/>
<wire x1="53.34" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="53.34" width="0.254" layer="94"/>
<wire x1="0" y1="53.34" x2="53.34" y2="53.34" width="0.254" layer="94"/>
<wire x1="53.34" y1="53.34" x2="53.34" y2="0" width="0.254" layer="94"/>
<text x="21.59" y="26.67" size="1.778" layer="94">nRF52832</text>
<text x="48.26" y="-2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="48.26" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="TWI-MILLMAX" urn="urn:adsk.eagle:symbol:725718/1" library_version="7" library_locally_modified="yes">
<pin name="VCC" x="-5.08" y="7.62" length="middle" rot="R270"/>
<pin name="GND" x="0" y="7.62" length="middle" rot="R270"/>
<pin name="SDA" x="5.08" y="7.62" length="middle" rot="R270"/>
<pin name="SCL" x="10.16" y="7.62" length="middle" rot="R270"/>
<wire x1="-7.62" y1="2.54" x2="12.7" y2="2.54" width="0.254" layer="94"/>
<wire x1="12.7" y1="2.54" x2="12.7" y2="-5.08" width="0.254" layer="94"/>
<wire x1="12.7" y1="-5.08" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="-7.62" y2="2.54" width="0.254" layer="94"/>
<text x="-7.62" y="-7.62" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="R-US" library_version="7" library_locally_modified="yes">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.2032" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.2032" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.2032" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="SWD_SERIAL" library_version="7" library_locally_modified="yes">
<pin name="VCC" x="5.08" y="-2.54" length="middle" rot="R180"/>
<pin name="GND" x="5.08" y="2.54" length="middle" rot="R180"/>
<pin name="SWDCLK" x="5.08" y="7.62" length="middle" rot="R180"/>
<pin name="SWDIO" x="5.08" y="12.7" length="middle" rot="R180"/>
<pin name="TX" x="5.08" y="17.78" length="middle" rot="R180"/>
<pin name="RX" x="5.08" y="22.86" length="middle" rot="R180"/>
<wire x1="2.54" y1="25.4" x2="2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="-5.08" x2="-12.7" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-5.08" x2="-12.7" y2="25.4" width="0.254" layer="94"/>
<wire x1="-12.7" y1="25.4" x2="2.54" y2="25.4" width="0.254" layer="94"/>
<text x="-12.7" y="22.86" size="1.27" layer="95">&gt;NAME</text>
</symbol>
<symbol name="TACTILE_SWITCH_4POS" library_version="7" library_locally_modified="yes">
<pin name="P$1" x="0" y="-7.62" length="point"/>
<pin name="P$3" x="0" y="0" length="point"/>
<pin name="P$4" x="20.32" y="0" length="middle" rot="R180"/>
<pin name="P$2" x="20.32" y="-7.62" length="middle" rot="R180"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-10.16" x2="17.78" y2="-10.16" width="0.254" layer="94"/>
<wire x1="17.78" y1="-10.16" x2="17.78" y2="2.54" width="0.254" layer="94"/>
<wire x1="17.78" y1="2.54" x2="-2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-7.62" x2="7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="-7.62" x2="15.24" y2="-7.62" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="7.62" y2="0" width="0.254" layer="94"/>
<wire x1="7.62" y1="0" x2="15.24" y2="0" width="0.254" layer="94"/>
<wire x1="7.62" y1="0" x2="7.62" y2="-2.54" width="0.254" layer="94"/>
<wire x1="7.62" y1="-7.62" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="10.16" y2="-2.54" width="0.254" layer="94"/>
<text x="3.048" y="3.556" size="1.778" layer="94">&gt;NAME</text>
</symbol>
<symbol name="LTC1844" library_version="7" library_locally_modified="yes">
<pin name="BYP" x="7.62" y="-2.54" length="middle"/>
<pin name="OUT" x="7.62" y="5.08" length="middle"/>
<pin name="VIN" x="-7.62" y="5.08" length="middle" rot="R180"/>
<pin name="NSHDN" x="-7.62" y="-2.54" length="middle" rot="R180"/>
<pin name="GND" x="0" y="-5.08" length="middle" rot="R270"/>
<wire x1="-10.16" y1="7.62" x2="-10.16" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-7.62" x2="10.16" y2="-7.62" width="0.254" layer="94"/>
<wire x1="10.16" y1="-7.62" x2="10.16" y2="7.62" width="0.254" layer="94"/>
<wire x1="10.16" y1="7.62" x2="-10.16" y2="7.62" width="0.254" layer="94"/>
<text x="-5.08" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="0" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="+3V3" library_version="7" library_locally_modified="yes">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+3V3" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="GND" library_version="7" library_locally_modified="yes">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="C-EU" library_version="7" library_locally_modified="yes">
<wire x1="0" y1="0" x2="0" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-2.032" width="0.1524" layer="94"/>
<text x="1.524" y="0.381" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-4.699" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-2.032" x2="2.032" y2="-1.524" layer="94"/>
<rectangle x1="-2.032" y1="-1.016" x2="2.032" y2="-0.508" layer="94"/>
<pin name="1" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
<symbol name="JST-CONNECT-ACH-2" library_version="7" library_locally_modified="yes">
<pin name="VCC" x="2.54" y="7.62" length="middle" rot="R180"/>
<pin name="GND" x="2.54" y="0" length="middle" rot="R180"/>
<wire x1="0" y1="10.16" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-10.16" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-2.54" x2="-10.16" y2="10.16" width="0.254" layer="94"/>
<wire x1="-10.16" y1="10.16" x2="0" y2="10.16" width="0.254" layer="94"/>
<text x="-12.7" y="10.16" size="2.54" layer="95">&gt;NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="BC832" library_version="7" library_locally_modified="yes">
<gates>
<gate name="G$1" symbol="BC832" x="0" y="0"/>
</gates>
<devices>
<device name="" package="FANSTEL_BC832">
<connects>
<connect gate="G$1" pin="DCC@47" pad="P$24"/>
<connect gate="G$1" pin="DEC4@46" pad="P$29"/>
<connect gate="G$1" pin="P0.00/XL1@02" pad="P$11"/>
<connect gate="G$1" pin="P0.01/XL2@03" pad="P$12"/>
<connect gate="G$1" pin="P0.02/AIN0@04" pad="P$13"/>
<connect gate="G$1" pin="P0.03/AIN1@05" pad="P$9"/>
<connect gate="G$1" pin="P0.04/AIN2@06" pad="P$19"/>
<connect gate="G$1" pin="P0.05/AIN3@07" pad="P$23"/>
<connect gate="G$1" pin="P0.06@08" pad="P$18"/>
<connect gate="G$1" pin="P0.07@09" pad="P$22"/>
<connect gate="G$1" pin="P0.08@10" pad="P$17"/>
<connect gate="G$1" pin="P0.09/NFC1@11" pad="P$27"/>
<connect gate="G$1" pin="P0.10/NFC2@12" pad="P$32"/>
<connect gate="G$1" pin="P0.11@14" pad="P$7"/>
<connect gate="G$1" pin="P0.12@15" pad="P$8"/>
<connect gate="G$1" pin="P0.13@16" pad="P$6"/>
<connect gate="G$1" pin="P0.14@17" pad="P$33"/>
<connect gate="G$1" pin="P0.15@18" pad="P$28"/>
<connect gate="G$1" pin="P0.16@19" pad="P$34"/>
<connect gate="G$1" pin="P0.17@20" pad="P$20"/>
<connect gate="G$1" pin="P0.18/SWO@21" pad="P$5"/>
<connect gate="G$1" pin="P0.19@22" pad="P$35"/>
<connect gate="G$1" pin="P0.20@23" pad="P$2"/>
<connect gate="G$1" pin="P0.21/RESET@24" pad="P$10"/>
<connect gate="G$1" pin="P0.22@27" pad="P$30"/>
<connect gate="G$1" pin="P0.23@28" pad="P$36"/>
<connect gate="G$1" pin="P0.24@29" pad="P$31"/>
<connect gate="G$1" pin="P0.25@37" pad="P$26"/>
<connect gate="G$1" pin="P0.26@38" pad="P$15"/>
<connect gate="G$1" pin="P0.27@39" pad="P$14"/>
<connect gate="G$1" pin="P0.28@40" pad="P$21"/>
<connect gate="G$1" pin="P0.29@41" pad="P$25"/>
<connect gate="G$1" pin="SWDCLK@25" pad="P$4"/>
<connect gate="G$1" pin="SWDIO@26" pad="P$3"/>
<connect gate="G$1" pin="VDD@48" pad="P$16"/>
<connect gate="G$1" pin="VSS@45" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SHINKD" package="FANSTEL_BC832_SHRINKD">
<connects>
<connect gate="G$1" pin="DCC@47" pad="P$24"/>
<connect gate="G$1" pin="DEC4@46" pad="P$29"/>
<connect gate="G$1" pin="P0.00/XL1@02" pad="P$11"/>
<connect gate="G$1" pin="P0.01/XL2@03" pad="P$12"/>
<connect gate="G$1" pin="P0.02/AIN0@04" pad="P$13"/>
<connect gate="G$1" pin="P0.03/AIN1@05" pad="P$9"/>
<connect gate="G$1" pin="P0.04/AIN2@06" pad="P$19"/>
<connect gate="G$1" pin="P0.05/AIN3@07" pad="P$23"/>
<connect gate="G$1" pin="P0.06@08" pad="P$18"/>
<connect gate="G$1" pin="P0.07@09" pad="P$22"/>
<connect gate="G$1" pin="P0.08@10" pad="P$17"/>
<connect gate="G$1" pin="P0.09/NFC1@11" pad="P$27"/>
<connect gate="G$1" pin="P0.10/NFC2@12" pad="P$32"/>
<connect gate="G$1" pin="P0.11@14" pad="P$7"/>
<connect gate="G$1" pin="P0.12@15" pad="P$8"/>
<connect gate="G$1" pin="P0.13@16" pad="P$6"/>
<connect gate="G$1" pin="P0.14@17" pad="P$33"/>
<connect gate="G$1" pin="P0.15@18" pad="P$28"/>
<connect gate="G$1" pin="P0.16@19" pad="P$34"/>
<connect gate="G$1" pin="P0.17@20" pad="P$20"/>
<connect gate="G$1" pin="P0.18/SWO@21" pad="P$5"/>
<connect gate="G$1" pin="P0.19@22" pad="P$35"/>
<connect gate="G$1" pin="P0.20@23" pad="P$2"/>
<connect gate="G$1" pin="P0.21/RESET@24" pad="P$10"/>
<connect gate="G$1" pin="P0.22@27" pad="P$30"/>
<connect gate="G$1" pin="P0.23@28" pad="P$36"/>
<connect gate="G$1" pin="P0.24@29" pad="P$31"/>
<connect gate="G$1" pin="P0.25@37" pad="P$26"/>
<connect gate="G$1" pin="P0.26@38" pad="P$15"/>
<connect gate="G$1" pin="P0.27@39" pad="P$14"/>
<connect gate="G$1" pin="P0.28@40" pad="P$21"/>
<connect gate="G$1" pin="P0.29@41" pad="P$25"/>
<connect gate="G$1" pin="SWDCLK@25" pad="P$4"/>
<connect gate="G$1" pin="SWDIO@26" pad="P$3"/>
<connect gate="G$1" pin="VDD@48" pad="P$16"/>
<connect gate="G$1" pin="VSS@45" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TWI-MILLMAX" urn="urn:adsk.eagle:component:725721/3" locally_modified="yes" prefix="J" uservalue="yes" library_version="7" library_locally_modified="yes">
<gates>
<gate name="G$1" symbol="TWI-MILLMAX" x="7.62" y="5.08"/>
</gates>
<devices>
<device name="" package="MILLMAX-22-004-40-001101">
<connects>
<connect gate="G$1" pin="GND" pad="4"/>
<connect gate="G$1" pin="SCL" pad="2"/>
<connect gate="G$1" pin="SDA" pad="1"/>
<connect gate="G$1" pin="VCC" pad="3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:725720/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="RECV" package="MILLMAX-22-004-40-001101_RECV">
<connects>
<connect gate="G$1" pin="GND" pad="4"/>
<connect gate="G$1" pin="SCL" pad="2"/>
<connect gate="G$1" pin="SDA" pad="1"/>
<connect gate="G$1" pin="VCC" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RESISTOR" library_version="7" library_locally_modified="yes">
<gates>
<gate name="G$1" symbol="R-US" x="0" y="0"/>
</gates>
<devices>
<device name="0805" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="1206_MILL">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TAGCONNECT_SWD_SERIAL" library_version="7" library_locally_modified="yes">
<gates>
<gate name="G$1" symbol="SWD_SERIAL" x="12.7" y="5.08"/>
</gates>
<devices>
<device name="" package="TAG-CONNECT">
<connects>
<connect gate="G$1" pin="GND" pad="P$2"/>
<connect gate="G$1" pin="RX" pad="P$5"/>
<connect gate="G$1" pin="SWDCLK" pad="P$6"/>
<connect gate="G$1" pin="SWDIO" pad="P$4"/>
<connect gate="G$1" pin="TX" pad="P$3"/>
<connect gate="G$1" pin="VCC" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TACTILE_SWITCH_KMR7" library_version="7" library_locally_modified="yes">
<gates>
<gate name="G$1" symbol="TACTILE_SWITCH_4POS" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TACTILE_SWITCH_KMR7">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
<connect gate="G$1" pin="P$3" pad="P$3"/>
<connect gate="G$1" pin="P$4" pad="P$4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LTC1844-3.3" library_version="7" library_locally_modified="yes">
<gates>
<gate name="G$1" symbol="LTC1844" x="0" y="2.54"/>
</gates>
<devices>
<device name="" package="TSOT-23-5">
<connects>
<connect gate="G$1" pin="BYP" pad="P$4"/>
<connect gate="G$1" pin="GND" pad="P$2"/>
<connect gate="G$1" pin="NSHDN" pad="P$3"/>
<connect gate="G$1" pin="OUT" pad="P$5"/>
<connect gate="G$1" pin="VIN" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+3V3" prefix="+3V3" library_version="7" library_locally_modified="yes">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="+3V3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" prefix="GND" library_version="7" library_locally_modified="yes">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAPACITOR" uservalue="yes" library_version="7" library_locally_modified="yes">
<description>&lt;b&gt;Generic chip capacitor&lt;/b&gt;</description>
<gates>
<gate name="C$1" symbol="C-EU" x="0" y="0"/>
</gates>
<devices>
<device name="_0201_L" package="RESC0201_L">
<connects>
<connect gate="C$1" pin="1" pad="1"/>
<connect gate="C$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0201_M" package="RESC0201_M">
<connects>
<connect gate="C$1" pin="1" pad="1"/>
<connect gate="C$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0201_N" package="RESC0201_N">
<connects>
<connect gate="C$1" pin="1" pad="1"/>
<connect gate="C$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0402_L" package="RESC0402_L">
<connects>
<connect gate="C$1" pin="1" pad="1"/>
<connect gate="C$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0402_M" package="RESC0402_M">
<connects>
<connect gate="C$1" pin="1" pad="1"/>
<connect gate="C$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0402_N" package="RESC0402_N">
<connects>
<connect gate="C$1" pin="1" pad="1"/>
<connect gate="C$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0603_L" package="RESC0603_L">
<connects>
<connect gate="C$1" pin="1" pad="1"/>
<connect gate="C$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0603_M" package="RESC0603_M">
<connects>
<connect gate="C$1" pin="1" pad="1"/>
<connect gate="C$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0603_N" package="RESC0603_N">
<connects>
<connect gate="C$1" pin="1" pad="1"/>
<connect gate="C$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0805_L" package="RESC0805_L">
<connects>
<connect gate="C$1" pin="1" pad="1"/>
<connect gate="C$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0805_M" package="RESC0805_M">
<connects>
<connect gate="C$1" pin="1" pad="1"/>
<connect gate="C$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0805_N" package="RESC0805_N">
<connects>
<connect gate="C$1" pin="1" pad="1"/>
<connect gate="C$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="1206_MILL">
<connects>
<connect gate="C$1" pin="1" pad="1"/>
<connect gate="C$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="JST-CONNECT-ACH-2" library_version="7" library_locally_modified="yes">
<gates>
<gate name="G$1" symbol="JST-CONNECT-ACH-2" x="10.16" y="2.54"/>
</gates>
<devices>
<device name="" package="JST-CONNECT-ACH-2">
<connects>
<connect gate="G$1" pin="GND" pad="P$1"/>
<connect gate="G$1" pin="VCC" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1" urn="urn:adsk.eagle:library:371">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26925/1" library_version="1">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="+3V3" urn="urn:adsk.eagle:symbol:26950/1" library_version="1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+3V3" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" urn="urn:adsk.eagle:component:26954/1" prefix="GND" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+3V3" urn="urn:adsk.eagle:component:26981/1" prefix="+3V3" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="+3V3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U$1" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="BC832" device="" value="BC832"/>
<part name="J1" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="TWI-MILLMAX" device="RECV"/>
<part name="J2" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="TWI-MILLMAX" device="RECV"/>
<part name="J3" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="TWI-MILLMAX" device="RECV"/>
<part name="J4" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="TWI-MILLMAX" device="RECV"/>
<part name="R4" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="RESISTOR" device="" value="2K"/>
<part name="R3" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="RESISTOR" device="" value="2K"/>
<part name="R2" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="RESISTOR" device="" value="2K"/>
<part name="R1" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="RESISTOR" device="" value="2K"/>
<part name="GND1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="+3V1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="+3V2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="U$2" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="TAGCONNECT_SWD_SERIAL" device=""/>
<part name="U$3" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="TACTILE_SWITCH_KMR7" device=""/>
<part name="R5" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="RESISTOR" device="" value="10K"/>
<part name="+3V3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="GND5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND6" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="+3V4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="U$4" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="LTC1844-3.3" device=""/>
<part name="+3V5" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="+3V3" device=""/>
<part name="GND7" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="GND" device=""/>
<part name="GND8" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="GND" device=""/>
<part name="C2" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="CAPACITOR" device="_0805_L" value="1uF"/>
<part name="C1" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="CAPACITOR" device="_0805_L" value=".1uF"/>
<part name="C3" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="CAPACITOR" device="_0805_L" value="1uF"/>
<part name="U$5" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="JST-CONNECT-ACH-2" device=""/>
<part name="C4" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="CAPACITOR" device=""/>
<part name="C5" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="CAPACITOR" device=""/>
<part name="GND9" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="GND" device=""/>
<part name="GND10" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="GND" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="U$1" gate="G$1" x="58.42" y="12.7" rot="R90"/>
<instance part="J1" gate="G$1" x="137.16" y="-12.7" rot="R90"/>
<instance part="J2" gate="G$1" x="137.16" y="17.78" rot="R90"/>
<instance part="J3" gate="G$1" x="137.16" y="48.26" rot="R90"/>
<instance part="J4" gate="G$1" x="137.16" y="78.74" rot="R90"/>
<instance part="R4" gate="G$1" x="101.6" y="68.58" rot="R90"/>
<instance part="R3" gate="G$1" x="88.9" y="68.58" rot="R90"/>
<instance part="R2" gate="G$1" x="104.14" y="5.08" rot="R90"/>
<instance part="R1" gate="G$1" x="91.44" y="5.08" rot="R90"/>
<instance part="GND1" gate="1" x="127" y="5.08"/>
<instance part="GND2" gate="1" x="127" y="-25.4"/>
<instance part="GND3" gate="1" x="124.46" y="35.56"/>
<instance part="GND4" gate="1" x="124.46" y="66.04"/>
<instance part="+3V1" gate="G$1" x="91.44" y="20.32"/>
<instance part="+3V2" gate="G$1" x="88.9" y="86.36"/>
<instance part="U$2" gate="G$1" x="-25.4" y="93.98"/>
<instance part="U$3" gate="G$1" x="27.94" y="104.14"/>
<instance part="R5" gate="G$1" x="66.04" y="111.76" rot="R90"/>
<instance part="+3V3" gate="G$1" x="66.04" y="127"/>
<instance part="GND5" gate="1" x="53.34" y="86.36"/>
<instance part="GND6" gate="1" x="-12.7" y="30.48"/>
<instance part="+3V4" gate="G$1" x="-22.86" y="30.48"/>
<instance part="U$4" gate="G$1" x="-38.1" y="50.8"/>
<instance part="+3V5" gate="G$1" x="-22.86" y="63.5"/>
<instance part="GND7" gate="1" x="-38.1" y="30.48"/>
<instance part="GND8" gate="1" x="-2.54" y="71.12"/>
<instance part="C2" gate="C$1" x="-15.24" y="66.04" rot="R90"/>
<instance part="C1" gate="C$1" x="-22.86" y="43.18"/>
<instance part="C3" gate="C$1" x="-15.24" y="55.88" rot="R90"/>
<instance part="U$5" gate="G$1" x="-35.56" y="73.66"/>
<instance part="C4" gate="C$1" x="-22.86" y="17.78"/>
<instance part="C5" gate="C$1" x="-12.7" y="17.78"/>
<instance part="GND9" gate="1" x="-22.86" y="10.16"/>
<instance part="GND10" gate="1" x="-12.7" y="10.16"/>
</instances>
<busses>
</busses>
<nets>
<net name="SCL2" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="SCL"/>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="129.54" y1="-2.54" x2="111.76" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="111.76" y1="-2.54" x2="104.14" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="104.14" y1="-2.54" x2="104.14" y2="0" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="SCL"/>
<wire x1="129.54" y1="27.94" x2="111.76" y2="27.94" width="0.1524" layer="91"/>
<wire x1="111.76" y1="27.94" x2="111.76" y2="-2.54" width="0.1524" layer="91"/>
<junction x="111.76" y="-2.54"/>
<label x="111.76" y="27.94" size="1.778" layer="95"/>
</segment>
</net>
<net name="SDA2" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="91.44" y1="-7.62" x2="91.44" y2="0" width="0.1524" layer="91"/>
<pinref part="J1" gate="G$1" pin="SDA"/>
<wire x1="129.54" y1="-7.62" x2="121.92" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="SDA"/>
<wire x1="121.92" y1="-7.62" x2="91.44" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="129.54" y1="22.86" x2="121.92" y2="22.86" width="0.1524" layer="91"/>
<wire x1="121.92" y1="22.86" x2="121.92" y2="-7.62" width="0.1524" layer="91"/>
<junction x="121.92" y="-7.62"/>
<label x="121.92" y="22.86" size="1.778" layer="95"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="J2" gate="G$1" pin="GND"/>
<wire x1="129.54" y1="17.78" x2="127" y2="17.78" width="0.1524" layer="91"/>
<wire x1="127" y1="17.78" x2="127" y2="7.62" width="0.1524" layer="91"/>
<pinref part="GND1" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="GND"/>
<wire x1="129.54" y1="-12.7" x2="127" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="127" y1="-12.7" x2="127" y2="-22.86" width="0.3048" layer="91"/>
<pinref part="GND2" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="J4" gate="G$1" pin="GND"/>
<pinref part="GND4" gate="1" pin="GND"/>
<wire x1="129.54" y1="78.74" x2="124.46" y2="78.74" width="0.1524" layer="91"/>
<wire x1="124.46" y1="78.74" x2="124.46" y2="68.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J3" gate="G$1" pin="GND"/>
<pinref part="GND3" gate="1" pin="GND"/>
<wire x1="129.54" y1="48.26" x2="124.46" y2="48.26" width="0.1524" layer="91"/>
<wire x1="124.46" y1="48.26" x2="124.46" y2="38.1" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$3" gate="G$1" pin="P$2"/>
<wire x1="48.26" y1="96.52" x2="53.34" y2="96.52" width="0.1524" layer="91"/>
<wire x1="53.34" y1="96.52" x2="53.34" y2="88.9" width="0.1524" layer="91"/>
<pinref part="GND5" gate="1" pin="GND"/>
<pinref part="U$3" gate="G$1" pin="P$1"/>
<wire x1="27.94" y1="96.52" x2="48.26" y2="96.52" width="0.1524" layer="91"/>
<junction x="48.26" y="96.52"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="VSS@45"/>
<wire x1="-12.7" y1="33.02" x2="0" y2="33.02" width="0.1524" layer="91"/>
<pinref part="GND6" gate="1" pin="GND"/>
<pinref part="C1" gate="C$1" pin="2"/>
<wire x1="-22.86" y1="38.1" x2="-12.7" y2="38.1" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="38.1" x2="-12.7" y2="33.02" width="0.1524" layer="91"/>
<junction x="-12.7" y="33.02"/>
</segment>
<segment>
<pinref part="GND7" gate="1" pin="GND"/>
<pinref part="U$4" gate="G$1" pin="GND"/>
<wire x1="-38.1" y1="33.02" x2="-38.1" y2="45.72" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND8" gate="1" pin="GND"/>
<wire x1="-10.16" y1="76.2" x2="-2.54" y2="76.2" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="76.2" x2="-2.54" y2="73.66" width="0.1524" layer="91"/>
<pinref part="C2" gate="C$1" pin="2"/>
<wire x1="-10.16" y1="66.04" x2="-10.16" y2="73.66" width="0.1524" layer="91"/>
<pinref part="C3" gate="C$1" pin="2"/>
<wire x1="-10.16" y1="73.66" x2="-10.16" y2="76.2" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="55.88" x2="-10.16" y2="66.04" width="0.1524" layer="91"/>
<junction x="-10.16" y="66.04"/>
<wire x1="-10.16" y1="76.2" x2="-10.16" y2="96.52" width="0.1524" layer="91"/>
<junction x="-10.16" y="76.2"/>
<pinref part="U$2" gate="G$1" pin="GND"/>
<wire x1="-10.16" y1="96.52" x2="-20.32" y2="96.52" width="0.1524" layer="91"/>
<pinref part="U$5" gate="G$1" pin="GND"/>
<wire x1="-33.02" y1="73.66" x2="-10.16" y2="73.66" width="0.1524" layer="91"/>
<junction x="-10.16" y="73.66"/>
</segment>
<segment>
<pinref part="C4" gate="C$1" pin="2"/>
<pinref part="GND9" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C5" gate="C$1" pin="2"/>
<pinref part="GND10" gate="1" pin="GND"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="104.14" y1="10.16" x2="104.14" y2="12.7" width="0.1524" layer="91"/>
<wire x1="104.14" y1="12.7" x2="91.44" y2="12.7" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="91.44" y1="12.7" x2="91.44" y2="10.16" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="VCC"/>
<wire x1="129.54" y1="12.7" x2="116.84" y2="12.7" width="0.1524" layer="91"/>
<junction x="104.14" y="12.7"/>
<wire x1="116.84" y1="12.7" x2="104.14" y2="12.7" width="0.1524" layer="91"/>
<wire x1="91.44" y1="12.7" x2="91.44" y2="17.78" width="0.1524" layer="91"/>
<junction x="91.44" y="12.7"/>
<pinref part="J1" gate="G$1" pin="VCC"/>
<wire x1="129.54" y1="-17.78" x2="116.84" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="116.84" y1="-17.78" x2="116.84" y2="12.7" width="0.1524" layer="91"/>
<junction x="116.84" y="12.7"/>
<pinref part="+3V1" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="J4" gate="G$1" pin="VCC"/>
<pinref part="R4" gate="G$1" pin="2"/>
<wire x1="129.54" y1="73.66" x2="114.3" y2="73.66" width="0.1524" layer="91"/>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="114.3" y1="73.66" x2="101.6" y2="73.66" width="0.1524" layer="91"/>
<wire x1="88.9" y1="73.66" x2="101.6" y2="73.66" width="0.1524" layer="91"/>
<junction x="101.6" y="73.66"/>
<pinref part="+3V2" gate="G$1" pin="+3V3"/>
<wire x1="88.9" y1="73.66" x2="88.9" y2="83.82" width="0.1524" layer="91"/>
<junction x="88.9" y="73.66"/>
<pinref part="J3" gate="G$1" pin="VCC"/>
<wire x1="129.54" y1="43.18" x2="114.3" y2="43.18" width="0.1524" layer="91"/>
<wire x1="114.3" y1="43.18" x2="114.3" y2="73.66" width="0.1524" layer="91"/>
<junction x="114.3" y="73.66"/>
</segment>
<segment>
<pinref part="R5" gate="G$1" pin="2"/>
<pinref part="+3V3" gate="G$1" pin="+3V3"/>
<wire x1="66.04" y1="116.84" x2="66.04" y2="124.46" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="VDD@48"/>
<wire x1="-22.86" y1="25.4" x2="-12.7" y2="25.4" width="0.1524" layer="91"/>
<pinref part="+3V4" gate="G$1" pin="+3V3"/>
<wire x1="-12.7" y1="25.4" x2="0" y2="25.4" width="0.1524" layer="91"/>
<wire x1="-22.86" y1="25.4" x2="-22.86" y2="27.94" width="0.1524" layer="91"/>
<pinref part="C4" gate="C$1" pin="1"/>
<wire x1="-22.86" y1="20.32" x2="-22.86" y2="25.4" width="0.1524" layer="91"/>
<junction x="-22.86" y="25.4"/>
<pinref part="C5" gate="C$1" pin="1"/>
<wire x1="-12.7" y1="20.32" x2="-12.7" y2="25.4" width="0.1524" layer="91"/>
<junction x="-12.7" y="25.4"/>
</segment>
<segment>
<pinref part="U$4" gate="G$1" pin="OUT"/>
<wire x1="-30.48" y1="55.88" x2="-22.86" y2="55.88" width="0.1524" layer="91"/>
<pinref part="+3V5" gate="G$1" pin="+3V3"/>
<wire x1="-22.86" y1="55.88" x2="-22.86" y2="60.96" width="0.1524" layer="91"/>
<pinref part="C3" gate="C$1" pin="1"/>
<wire x1="-22.86" y1="55.88" x2="-17.78" y2="55.88" width="0.1524" layer="91"/>
<junction x="-22.86" y="55.88"/>
</segment>
</net>
<net name="SDA1" class="0">
<segment>
<pinref part="J3" gate="G$1" pin="SDA"/>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="129.54" y1="53.34" x2="119.38" y2="53.34" width="0.1524" layer="91"/>
<wire x1="119.38" y1="53.34" x2="88.9" y2="53.34" width="0.1524" layer="91"/>
<wire x1="88.9" y1="53.34" x2="88.9" y2="63.5" width="0.1524" layer="91"/>
<pinref part="J4" gate="G$1" pin="SDA"/>
<wire x1="129.54" y1="83.82" x2="119.38" y2="83.82" width="0.1524" layer="91"/>
<wire x1="119.38" y1="83.82" x2="119.38" y2="53.34" width="0.1524" layer="91"/>
<junction x="119.38" y="53.34"/>
<label x="119.38" y="83.82" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="P0.26@38"/>
<wire x1="0" y1="50.8" x2="-5.08" y2="50.8" width="0.1524" layer="91"/>
<label x="-10.16" y="50.8" size="1.778" layer="95"/>
</segment>
</net>
<net name="SCL1" class="0">
<segment>
<pinref part="J4" gate="G$1" pin="SCL"/>
<wire x1="129.54" y1="88.9" x2="109.22" y2="88.9" width="0.1524" layer="91"/>
<pinref part="J3" gate="G$1" pin="SCL"/>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="129.54" y1="58.42" x2="109.22" y2="58.42" width="0.1524" layer="91"/>
<wire x1="109.22" y1="58.42" x2="101.6" y2="58.42" width="0.1524" layer="91"/>
<wire x1="101.6" y1="58.42" x2="101.6" y2="63.5" width="0.1524" layer="91"/>
<wire x1="109.22" y1="88.9" x2="109.22" y2="58.42" width="0.1524" layer="91"/>
<junction x="109.22" y="58.42"/>
<label x="109.22" y="88.9" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="P0.27@39"/>
<wire x1="0" y1="48.26" x2="-5.08" y2="48.26" width="0.1524" layer="91"/>
<label x="-10.16" y="48.26" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="SWDIO@26"/>
<wire x1="43.18" y1="88.9" x2="43.18" y2="71.12" width="0.1524" layer="91"/>
<pinref part="U$2" gate="G$1" pin="SWDIO"/>
<wire x1="-20.32" y1="106.68" x2="0" y2="106.68" width="0.1524" layer="91"/>
<wire x1="0" y1="106.68" x2="0" y2="88.9" width="0.1524" layer="91"/>
<wire x1="0" y1="88.9" x2="43.18" y2="88.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="SWDCLK@25"/>
<wire x1="45.72" y1="71.12" x2="45.72" y2="81.28" width="0.1524" layer="91"/>
<pinref part="U$2" gate="G$1" pin="SWDCLK"/>
<wire x1="-20.32" y1="101.6" x2="-5.08" y2="101.6" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="101.6" x2="-5.08" y2="81.28" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="81.28" x2="45.72" y2="81.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="RX" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="RX"/>
<wire x1="-20.32" y1="116.84" x2="-7.62" y2="116.84" width="0.1524" layer="91"/>
<label x="-7.62" y="116.84" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="P0.06@08"/>
<wire x1="35.56" y1="7.62" x2="35.56" y2="-2.54" width="0.1524" layer="91"/>
<label x="35.56" y="-2.54" size="1.778" layer="95"/>
</segment>
</net>
<net name="TX" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="TX"/>
<wire x1="-20.32" y1="111.76" x2="-7.62" y2="111.76" width="0.1524" layer="91"/>
<label x="-7.62" y="111.76" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="P0.08@10"/>
<wire x1="40.64" y1="7.62" x2="40.64" y2="-2.54" width="0.1524" layer="91"/>
<label x="40.64" y="-2.54" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P0.21/RESET@24"/>
<wire x1="63.5" y1="53.34" x2="66.04" y2="53.34" width="0.1524" layer="91"/>
<wire x1="66.04" y1="53.34" x2="66.04" y2="104.14" width="0.1524" layer="91"/>
<pinref part="U$3" gate="G$1" pin="P$4"/>
<wire x1="66.04" y1="104.14" x2="48.26" y2="104.14" width="0.1524" layer="91"/>
<wire x1="66.04" y1="104.14" x2="66.04" y2="106.68" width="0.1524" layer="91"/>
<junction x="66.04" y="104.14"/>
<pinref part="R5" gate="G$1" pin="1"/>
<pinref part="U$3" gate="G$1" pin="P$3"/>
<wire x1="27.94" y1="104.14" x2="48.26" y2="104.14" width="0.1524" layer="91"/>
<junction x="48.26" y="104.14"/>
</segment>
</net>
<net name="VIN" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="VCC"/>
<wire x1="-17.78" y1="66.04" x2="-20.32" y2="66.04" width="0.1524" layer="91"/>
<pinref part="U$4" gate="G$1" pin="VIN"/>
<wire x1="-20.32" y1="66.04" x2="-53.34" y2="66.04" width="0.1524" layer="91"/>
<wire x1="-53.34" y1="66.04" x2="-53.34" y2="55.88" width="0.1524" layer="91"/>
<wire x1="-53.34" y1="55.88" x2="-45.72" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$4" gate="G$1" pin="NSHDN"/>
<wire x1="-45.72" y1="48.26" x2="-53.34" y2="48.26" width="0.1524" layer="91"/>
<wire x1="-53.34" y1="48.26" x2="-53.34" y2="55.88" width="0.1524" layer="91"/>
<junction x="-53.34" y="55.88"/>
<pinref part="C2" gate="C$1" pin="1"/>
<label x="-53.34" y="66.04" size="1.778" layer="95"/>
<wire x1="-20.32" y1="91.44" x2="-20.32" y2="81.28" width="0.1524" layer="91"/>
<junction x="-20.32" y="66.04"/>
<pinref part="U$5" gate="G$1" pin="VCC"/>
<wire x1="-20.32" y1="81.28" x2="-20.32" y2="66.04" width="0.1524" layer="91"/>
<wire x1="-33.02" y1="81.28" x2="-20.32" y2="81.28" width="0.1524" layer="91"/>
<junction x="-20.32" y="81.28"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="C1" gate="C$1" pin="1"/>
<wire x1="-22.86" y1="45.72" x2="-22.86" y2="48.26" width="0.1524" layer="91"/>
<pinref part="U$4" gate="G$1" pin="BYP"/>
<wire x1="-22.86" y1="48.26" x2="-30.48" y2="48.26" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
