#!/usr/bin/env python
import numpy as np
from math import *
import serial, time
import struct
import sys
import socket
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import bitstring

n_samples = 20
Nx = 100
Ny = 100
halls = np.zeros((Nx,Ny,5))

def roll_left(lst):
	return lst[1:] + [lst[0]]
def read_as5013_12bit(bytes):
	return (bitstring.Bits(bytes=bytes, length=16)<<4).unpack('uint:16')[0]
	#signed 12 bit val comes in two bytes
	#return struct.unpack("<h",bytearray([ (high<<4) , low<<4 ]))[0]
def read_int16(bytes,signed=False):
	try:
		n = struct.unpack("<h",bytearray(bytes))[0] if signed else struct.unpack("<H",bytearray(bytes))[0]
		return n
	except:
		print "not properly formatted bytes: ",list(bytes)
		return None


def read(ser=None):
	if ser is None:
		print "No serial connection!"
	else:
		#wait for framing signal at tail of packet
		framing = range(8) #we expect the sequence 012...8
		c = [ser.read(1) for i in range(n_samples+4)]
		c += [ser.read(1) for i in framing]
		while( map(ord,c[n_samples+4:]) != framing ):
			c = roll_left(c)
			c[-1] = ser.read(1)
		#print c
		#c = ser.read(n_samples+12) #samples (20) + xi (2) + yi (2) + framing (8)
		try:
			data = [read_as5013_12bit(c[x:x+2]) for x in xrange(0,n_samples,2)]
			data += [read_int16(c[x:x+2]) for x in xrange(n_samples,n_samples+4,2)]
		except(bitstring.CreationError):
			print c
		return data

def main():
	try:
		#make sure timeout is longer than any period between data.
		ser = serial.Serial(port='/dev/tty.usbserial-FT9L3KWR',baudrate=1000000,timeout=5.)
		print "Established serial connection"
		ser.flush()
		while True: 
			try:
				data = np.asarray(read(ser=ser))
				print data[10], data[11], data[1:10:2]-data[:10:2]
				if (data[10]<Nx) and (data[11]<Ny):
					halls[data[10],data[11]] = data[1:10:2] - data[:10:2]
			except(KeyboardInterrupt):
				ser.close()
				break
	except(OSError):
		#no serial port
		print "Couldn't find the serial port."
		ser = None			
	print "saving data"
	
	np.save('hall_readings.npy',halls)
	print "quitting"

if __name__ == '__main__':
	main()
