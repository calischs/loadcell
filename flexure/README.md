This is a simulation script for designing the flexure plates.

To run it, you'll need to install my pyFrame3dd wrapper:

https://gitlab.cba.mit.edu/calischs/pyframe3dd

then run 
```
python flexure_stiffness.py -M visualize
```
to see a matplotlib visualization, or run 
```
python flexure_stiffness.py -M simulate
```
to run simulations with the default values.  Example commands are included in CMD.